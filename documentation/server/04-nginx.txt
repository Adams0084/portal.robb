NGINX
    sudo add-apt-repository ppa:nginx/stable
    sudo apt update
    sudo apt install nginx

    add this to the configuration file @  /etc/nginx/sites-available/default

            location / {
                    proxy_buffering off;
                    proxy_pass  http://localhost:9000/;
                    proxy_redirect off;
                    proxy_set_header   X-Real-IP  $remote_addr;
                    proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
                    proxy_set_header   Host  $http_host;
            ....

            delete try_files

    edit /etc/nginx/nginx.conf
        * uncomment line
        #server_tonkens off;

    sudo service nginx start
#!/usr/bin/env bash

echo catalog version in - history/
cp portal-*.jar portal.jar

mv portal-*.jar history/

echo hz-portal service - stop
sudo service hz-portal stop

echo copy executable to running folder /usr/bin/hz-portal
sudo cp portal.jar /usr/bin/hz-portal

echo hz-portal service - start
sudo service hz-portal start

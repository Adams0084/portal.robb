--liquibase formatted sql
--changeset author:Robb
CREATE TABLE files (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    file_type varchar(20) NOT NULL,
    file_name varchar(512) NOT NULL,
    meta_data varchar(1024) NOT NULL,
    hash varchar(256) NOT NULL,
    start DATETIME(3) NULL,
    end DATETIME(3) NULL,
    status varchar(10) NOT NULL
);

CREATE INDEX files__account ON files (account);

CREATE INDEX files__hash ON files (hash);

CREATE INDEX files__status ON files (status);

CREATE INDEX files__processing_start ON files (start);

CREATE INDEX files__processing_end ON files (end);

CREATE INDEX files__file_name ON files (file_name);

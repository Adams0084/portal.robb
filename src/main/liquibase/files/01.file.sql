--liquibase formatted sql
--changeset author:Robb
ALTER TABLE files DROP index files__hash;
ALTER TABLE files DROP COLUMN hash;

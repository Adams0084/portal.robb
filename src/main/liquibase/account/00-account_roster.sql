--liquibase formatted sql
--changeset author:Robb
CREATE TABLE account_roster (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    file int NULL,

    user int NULL,
    first VARCHAR(75) NULL,
    last VARCHAR(75) NULL,
    nick VARCHAR(75) NULL,
    email VARCHAR(255) NULL,
    dob DATE NULL,
    male BIT NULL,
    height VARCHAR(15) NULL,
    id_local VARCHAR(50) NULL,
    weight VARCHAR(10) NULL,
    data VARCHAR(4096) NULL
);

CREATE INDEX account_roster__account ON account_roster (account);

CREATE INDEX account_roster__user ON account_roster (user);

CREATE INDEX account_roster__email ON account_roster (email);

CREATE INDEX account_roster__id_local ON account_roster (id_local);

--liquibase formatted sql
--changeset author:Robb
CREATE TABLE account (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    uid binary(16) NOT NULL,
    account_type varchar(50) NULL,
    name varchar(50) NULL,
    address_1 varchar(100) NULL,
    address_2 varchar(100) NULL,
    city  varchar(50) NULL,
    state char(2) NULL,
    zip varchar(20) NULL,
    phone varchar(20)
);

CREATE INDEX account__account_type ON account (account_type);

CREATE INDEX account__name ON account (name);

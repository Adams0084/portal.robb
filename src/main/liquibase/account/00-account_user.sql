--liquibase formatted sql
--changeset author:Robb
CREATE TABLE account_user (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    user int,
    role VARCHAR (25)
);

CREATE INDEX account_user__account ON account_user (account);

CREATE INDEX account_user__user ON account_user (user);

CREATE UNIQUE INDEX account_user__account_user_role ON account_user (account,user,role);

CREATE INDEX account_user__account_user_deleted ON account_user (account, user, deleted);

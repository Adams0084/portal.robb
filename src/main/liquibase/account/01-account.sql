--liquibase formatted sql
--changeset author:Robb
UPDATE account SET phone = NULL;

ALTER TABLE account
MODIFY COLUMN phone varchar(50);

--liquibase formatted sql
--changeset author:Robb
CREATE TABLE account_license (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    name varchar(75),
    expires DATE NOT NULL,
    features varchar(512)
);

CREATE INDEX account_license__account ON account_license (account);

CREATE INDEX account_license__expires ON account_license (expires);

CREATE INDEX account_license__account_expires_deleted ON account_license (account, expires, deleted);

--liquibase formatted sql
--changeset author:Robb
CREATE TABLE device_lease (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    device int NULL,
    roster int NULL,
    start DATETIME(3) NOT NULL,
    end DATETIME(3) NOT NULL
);

CREATE INDEX device_lease__device ON device_lease (device);

CREATE INDEX device_lease__roster ON device_lease (roster);

CREATE INDEX device_lease__deleted ON device_lease (deleted);

CREATE INDEX device_lease__start ON device_lease (start);

CREATE INDEX device_lease__end ON device_lease (end);

--liquibase formatted sql
--changeset author:Robb
CREATE TABLE device (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    uid binary(16) NOT NULL
);

CREATE INDEX device__account ON device (account);

CREATE INDEX device__deleted ON device (deleted);
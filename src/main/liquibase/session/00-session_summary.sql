--liquibase formatted sql
--changeset author:Robb
CREATE TABLE session_summary (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    file int NULL,

    session INT NULL,
    account_roster INT NOT NULL,
    start DATETIME(3) NOT NULL,
    end DATETIME(3) NOT NULL,

    fit VARCHAR(25) NULL,
    hrm VARCHAR(15) NULL,
    hz_max VARCHAR(15) NULL,
    hz_shr VARCHAR(15) NULL,
    hz_t1 VARCHAR(15) NULL,
    hz_t2 VARCHAR(15) NULL,
    pwr VARCHAR(15) NULL,
    sc VARCHAR(15) NULL,
    sdm VARCHAR(15) NULL,
    spd VARCHAR(15) NULL,
    step_max VARCHAR(15) NULL,
    cad VARCHAR(15) NULL,
    data VARCHAR(4096) NULL
);

CREATE INDEX session_summary__account ON session_summary(account);

CREATE INDEX session_summary__start ON session_summary(start);

CREATE INDEX session_summary__end ON session_summary(end);

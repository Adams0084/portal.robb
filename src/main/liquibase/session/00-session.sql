--liquibase formatted sql
--changeset author:Robb
CREATE TABLE session (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    account INT NOT NULL,

    name VARCHAR(100) NULL,
    location VARCHAR(100) NULL,
    teacher INT NULL,
    start DATETIME(3) NOT NULL,
    end DATETIME(3) NOT NULL
);

CREATE INDEX session__account ON session(account);

CREATE INDEX session__start ON session(start);

CREATE INDEX session__end ON session(end);

CREATE INDEX session__name ON session(name);

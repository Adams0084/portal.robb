--liquibase formatted sql
--changeset author:Robb
CREATE TABLE magic_link (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    link binary(16) NOT NULL,
    action VARCHAR (35) NOT NULL,
    expires DATETIME(3) NULL,
    used DATETIME(3) NULL,
    data VARCHAR(1024) NULL
);

CREATE INDEX magic_link__link ON magic_link (link);

CREATE INDEX magic_link__used ON magic_link (used);

CREATE INDEX magic_link__expires ON magic_link (expires);

CREATE INDEX magic_link__action ON magic_link (action);

--liquibase formatted sql
--changeset author:Robb
ALTER TABLE email DROP COLUMN attachments;
ALTER TABLE email ADD COLUMN template varchar(125) NULL;
ALTER TABLE email ADD COLUMN status varchar(15) NOT NULL;
ALTER TABLE email ADD COLUMN data varchar(4096) NOT NULL;

CREATE INDEX email__status ON email (status);

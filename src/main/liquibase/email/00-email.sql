--liquibase formatted sql
--changeset author:Robb
CREATE TABLE email (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    sent DATETIME(3) NULL,

    recipiant VARCHAR(255) NOT NULL,
    sender VARCHAR(255) NOT NULL,
    cc VARCHAR(255) NOT NULL,
    subject varchar(512) NOT NULL,
    body varchar(8096) NOT NULL,
    attachments varchar(4096) NULL
);

CREATE INDEX email__sent ON email (sent);

CREATE INDEX email__created ON email (created);

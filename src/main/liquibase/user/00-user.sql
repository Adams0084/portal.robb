--liquibase formatted sql
--changeset author:Robb
CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

    created DATETIME(3) NOT NULL,
    create_user int NOT NULL,
    updated DATETIME(3) NULL,
    update_user int NULL,
    deleted bit NOT NULL DEFAULT 0,

    avatar varchar(255) NULL,

    verified DATETIME(3) NULL,
    first varchar(100) NULL,
    last varchar(100) NULL,
    nick varchar(100) NULL,

    middle varchar(35) NULL,
    username varchar(255) NULL,
    display varchar(80) NULL,
    admin bit NOT NULL DEFAULT 0,
    external_id varchar(125) NULL,
    password varchar(255) NULL,
    last_logged_in DATETIME(3) NULL,

    preferences varchar(1024) NOT NULL DEFAULT '{}',

    optional_info bit NOT NULL DEFAULT 0,
    terms_agreed DATETIME(3) NULL,

    phone varchar(25) NULL,
    address_1 varchar(125) NULL,
    address_2 varchar(125) NULL,
    city varchar(50) NULL,
    state varchar(10) NULL,
    zip varchar(15) NULL,

    dob varchar(10) NULL,
    male BIT NULL
);

CREATE INDEX user__username ON user (username);

CREATE INDEX user__username_password ON user (username, password);

CREATE INDEX user__user_delete ON user (id, deleted);

--liquibase formatted sql
--changeset author:Robb
UPDATE user SET phone = NULL;

ALTER TABLE user
MODIFY COLUMN phone varchar(50);

package com.heartzones;

import com.heartzones.processor.EmailProcessor;
import com.heartzones.processor.FileProcessor;
import com.heartzones.startup.InitialData;
import com.heartzones.startup.JVM;
import com.heartzones.startup.Liquibase;
import com.heartzones.startup.jetty.JettyInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class App {
    private final static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws Exception {
        ApplicationProperties applicationProperties = new ApplicationProperties(App.class);
        applicationProperties.initialize();

        if (args.length > 0) applicationProperties.load(new File(args[0]));

        JVM jvm = new JVM();
        jvm.start(applicationProperties);

        Liquibase liquibase = new Liquibase();
        liquibase.start(applicationProperties);

        InitialData initialData = new InitialData();
        initialData.start(applicationProperties);

        /* start static file processor */
        FileProcessor.start(applicationProperties);
        EmailProcessor.start(applicationProperties);

        /* servlet should be started last */
        JettyInitializer jetty = new JettyInitializer(applicationProperties);
        jetty.start();
    }
}

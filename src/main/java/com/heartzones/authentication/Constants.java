package com.heartzones.authentication;

public class Constants {
    public final static String authenticated = "authenticated";
    public final static String verified = "verified";

    public final static String sessionStart = "session.start";
    public final static String sessionExpires = "session.expiration";
    public final static String sessionId = "session.id";

    public final static String userId = "id";
    public final static String userName = "user";

    public final static String firstName = "first";
    public final static String lastName = "last";
    public final static String displayName = "display";

    public final static String roles = "roles";
    public final static String accountCurrent = "account.id";
    public final static String accountCurrentName = "account.name";
    public final static String accountCurrentLincse = "account.license.name";
    public final static String accountCurrentLincseExpires = "account.license.expires";
}

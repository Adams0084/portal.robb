package com.heartzones.authentication;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class Session {
    private final static Logger logger = LoggerFactory.getLogger(com.heartzones.authentication.Session.class);

    public SessionContext sessionContext = null;
    public Request Request = null;

    public Session() {}
    public Session initialize(ApplicationProperties properties) {
        sessionContext = new SessionContext(properties);
        Request = new Request();
        return this;
    }

    public String sessionId() {
        return sessionContext.threadSessionId.get();
    }
    public String requestId() { return Request.threadRequestId.get(); }

    public String userName() {
        return get(Constants.userName);
    }
    public Integer userId() {
        return get(Constants.userId);
    }
    public Boolean isAuthenticated() { return get(Constants.authenticated, false); }

    public void init() {
        MDC.remove("requestId");
        MDC.remove("sessionId");
        MDC.remove("user");

        sessionContext.init();
        Request.init();
    }
    public void clear() {
        sessionContext.clear();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return sessionContext.get(key);
    }

    public <T> T get(String key, T defaultValue) {
        return sessionContext.get(key, defaultValue);
    }

    void set(String key, Object value) {
        sessionContext.set(key,value);
    }

    public void put(String key, ArrayList value) {
        if (value == null || value.size() == 0) {
            remove(key);
        } else {
            Object val = value.get(0);
            if (val instanceof String || val instanceof Integer || val instanceof Boolean || val instanceof Long || val instanceof Float) {
                set(key, value.toArray());
            } else {
                throw new RuntimeException(String.format("Illegal Type Exception for SessionContext key [%s]", key));
            }
        }
    }

    public void put(String key, String value) {
        if (value == null || value.trim().equals("")) {
            remove(key);
        } else {
            set(key, value.trim());
        }
    }

    public void put(String key, String[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Long value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Long[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Integer value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Integer[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Float value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Float[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Boolean value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void remove(String key) {
        sessionContext.remove(key);
    }

    public List<String> rolesList() {
        Object oRoles = get(Constants.roles);
        return roles_parse(oRoles);
    }

    public boolean roleCheck(String role) {
        List<String> roles = rolesList();
        return roles.contains(role);
    }

    private List<String> roles_parse(Object oRoles) {
        if (oRoles == null) {
            return new ArrayList<>();
        }
        if (oRoles instanceof String) {
            return Arrays.asList(((String) oRoles).split(","));
        }
        if (oRoles instanceof String[]) {
            return Arrays.asList((String[]) oRoles);
        }
        if (oRoles instanceof Object[]) {
            List<String> rtn = new ArrayList<>();
            Object[] arr = (Object[]) oRoles;
            for (Object x : arr) {
                rtn.add((String) x);
            }
            return rtn;
        }
        if (oRoles instanceof ArrayList) {
            return (ArrayList) oRoles;
        }
        return new ArrayList<>();
    }

    private Map<String, Object> version = new HashMap<>();
    public Map<String, Object> publicData(ApplicationProperties properties) {
        Map<String, Object> rtn = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        Gson gson = new Gson();
        if (this.version.size() == 0) {
            try {
                InputStream version = Session.class.getResourceAsStream("/version.json");
                this.version = gson.fromJson(new BufferedReader(new InputStreamReader(version, "UTF-8")), this.version.getClass());
                this.version.remove("artifact");
                this.version.remove("group");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        rtn.putAll(this.version);

        rtn.put(Constants.sessionId, sessionId());
        rtn.put(Constants.sessionExpires, get(Constants.sessionExpires));
        rtn.put(Constants.authenticated, isAuthenticated());
        rtn.put(Constants.roles, roles_parse(get(Constants.roles)));
        rtn.put(Constants.displayName, get(Constants.displayName));
        rtn.put(Constants.firstName, get(Constants.firstName));
        rtn.put(Constants.lastName, get(Constants.lastName));
        rtn.put(Constants.sessionStart, get(Constants.sessionStart));
        rtn.put(Constants.accountCurrent, get(Constants.accountCurrent));
        rtn.put(Constants.accountCurrentName, get(Constants.accountCurrentName));
        rtn.put(Constants.accountCurrentLincse, get(Constants.accountCurrentLincse));
        rtn.put(Constants.accountCurrentLincseExpires, get(Constants.accountCurrentLincseExpires));

        rtn.put(Constants.verified, get(Constants.verified));
        rtn.put(Constants.sessionExpires, get(Constants.sessionExpires));

        return rtn;
    }
}
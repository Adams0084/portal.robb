package com.heartzones.authentication;

import java.util.Map;

interface SessionCache {
    String name();
    Map<String,Object> get(String sId);
    void save(String sId, Map<String, Object> session);
    void delete(String sId);
    Boolean test();
    Boolean init();
    void flush();
}

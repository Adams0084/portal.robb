package com.heartzones.authentication;

import com.heartzones.ApplicationProperties;

import java.util.Map;

interface SessionStrategy {
    Boolean load(SessionContext sessionContext, ApplicationProperties properties, Map<String, String> data);
}

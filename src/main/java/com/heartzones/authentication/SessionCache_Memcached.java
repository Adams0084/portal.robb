package com.heartzones.authentication;

import com.google.common.base.Strings;
import com.heartzones.ApplicationProperties;
import com.heartzones.data.DataTools;
import net.spy.memcached.MemcachedClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

class SessionCache_Memcached implements SessionCache {
    private final static Logger logger = LoggerFactory.getLogger(SessionCache_Memcached.class);

    private static MemcachedClient cache;

    private Integer timeout = null;
    private String host = null;
    private Integer port = null;

    SessionCache_Memcached(ApplicationProperties properties) {
        timeout = Integer.parseInt(properties.get("session.timeout", "45")) + 2;
        host = properties.get("session.cache.host");
        port = Integer.parseInt(properties.get("session.cache.port"));
    }

    @Override
    public String name() {
        return "memcached";
    }

    @Override
    public Map<String, Object> get(String sId) {
        if (cache == null) {
            init();
        }

        Map<String, Object> session = null;
        if (!Strings.isNullOrEmpty(sId)) {
            session = (Map<String, Object>) cache.get(sId);
            if (session != null) {
                logger.info(String.format("[%s] Loaded", sId));
            } else {
                logger.info(String.format("[%s] not found", sId));
            }
        }

        return session;
    }

    @Override
    public void save(String sId, Map<String,Object> session) {
        if (cache == null) {
            init();
        }

        logger.info(String.format("[%s] Save [%s]", sId, session == null ? "NULL SessionContext" : session.get(Constants.userName) == null ? "NULL USER" : session.get(Constants.userName)));

        Date expires = (new Date((new Date()).getTime() + (Long.valueOf(timeout) * 1000 * 60)));
        session.put(Constants.sessionExpires, DataTools.Dates.formatISODateTime(expires));
        cache.set(sId, timeout * 60, session);
    }

    @Override
    public void delete(String sId) {
        if (cache == null) {
            init();
        }
        if (sId == null) return;
        cache.delete(sId);
    }

    @Override
    public Boolean test() {
        try {
            if (cache == null) {
                init();
            }
            if (cache == null) {
                return false;
            }

            UUID id = UUID.randomUUID();
            Map<String, Object> session = new HashMap<>();
            session.put("Test", "Cache");
            cache.set(id.toString(), 60, session);
            Object rtn = cache.get(id.toString());
            if (rtn == null) {
                logger.error("SessionCache failed to retrieve test data.");
                return false;
            }
            delete(id.toString());
            return true;
        }
        catch (Exception ex) {
            logger.error("SessionCache test failed.", ex);
            return false;
        }
    }

    @Override
    public Boolean init() {
        Boolean rtn = false;
        try {
            InetSocketAddress memCacheSocket = new InetSocketAddress(host, port);
            cache = new MemcachedClient(memCacheSocket);

            if (test()) {
                rtn = true;
            } else {
                cache = null;
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return rtn;
    }

    @Override
    public void flush() {
        if (cache == null) {
            init();
        }

        if (cache != null)
            cache.flush();
    }
}
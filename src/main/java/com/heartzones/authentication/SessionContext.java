package com.heartzones.authentication;

import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.*;

public class SessionContext {
    private final static Logger logger = LoggerFactory.getLogger(SessionContext.class);

    private List<SessionStrategy> _sessionStrategies = new LinkedList<>();
    private Map<String, SessionCache> _cache = new HashMap<>();

    static final ThreadLocal<Map<String, Object>> threadSession = new ThreadLocal<>();
    static final ThreadLocal<String> threadSessionId = new ThreadLocal<>();

    private ApplicationProperties properties;

    public SessionContext(ApplicationProperties properties) {
        this.properties = properties;
        _sessionStrategies.add(new SessionStrategy_Cookie_Session());
        _sessionStrategies.add(new SessionStrategy_Header_Session());

        _cache.put("memcached", new SessionCache_Memcached(properties));
    }

    String id() {
        return threadSessionId.get();
    }
    String id(String value) {
        if (value == null) value = UUID.randomUUID().toString();

        threadSessionId.set(value);
        if (threadSession.get() == null) threadSession.set(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));

        MDC.put("sessionId", value);
        
        return id();
    }
    public void load() {
        load((String) null);
    }
    public void load(String sId) {
        clear();

        Map<String, Object> session = null;
        //get session from cache
        if (sId == null) {
            session = sessionEmpty();
        } else {
            session = sessionGet(sId);
        }

        threadSession.set(session);
        id(session.get(Constants.sessionId) == null ? sId : (String) session.get(Constants.sessionId));
    }
    public void load(Map<String, String> keys) {
        if (keys != null && keys.size() > 0) {
            for (SessionStrategy sl : _sessionStrategies) {
                if (sl.load(this, properties, keys) != null) break;
            }
        }

        if (threadSessionId.get() == null)
            load();

        Integer userId =  get(Constants.userId);
        if (userId != null)
            MDC.put("user", userId.toString());
    }

    Map<String, Object> sessionEmpty() {
        Map<String, Object> rtn = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        if (threadSessionId.get() == null) {
            rtn.put(Constants.sessionId, UUID.randomUUID().toString());
        } else {
            rtn.put(Constants.sessionId, threadSessionId.get());
        }

        rtn.put(Constants.authenticated, false);

        return rtn;
    }

    Map<String, Object> sessionGet(String sId) {
        Map<String, Object> session = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        if (sId != null) {
            SessionCache cache = _cache.get(properties.get("session.cache.type"));
            session = cache.get(sId);
        }
            /* Initiate empty session if not found already */
        if (session == null || session.size() == 0) {
            session = sessionEmpty();
            session.put(Constants.sessionId, session.get(Constants.sessionId));
        }
        else {
            session.put(Constants.sessionId, sId);
        }

        return session;
    }

    public Boolean test() {
        SessionCache cache = _cache.get(properties.get("session.cache.type"));
        return cache.test();
    }

    void save(String sId, Map<String, Object> session) {
        if (sId != null) {
            SessionCache cache = _cache.get(properties.get("session.cache.type"));
            if (session == null || session.size() == 0) cache.delete(sId);
            else cache.save(sId, session);
        }
    }

    static String value(Map<String, String> data, String key) {
        if (data == null) return null;
        String rtn = null;
        Set<String> keys = data.keySet();
        for (String k : keys) {
            if (k.equalsIgnoreCase(key)) {
                rtn = data.get(k);
                break;
            }
        }
        return rtn;
    }

    public void save() {
        save(threadSessionId.get(), threadSession.get());
    }

    void init() {
        threadSessionId.remove();
        threadSession.remove();
    }
    void clear() {
        threadSession.remove();
    }

    public void delete() {
        SessionCache cache = _cache.get(properties.get("session.cache.type"));
        cache.delete(threadSessionId.get());
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        try {
            if (threadSession.get() == null) threadSession.set(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));

            Object v = threadSession.get().get(key);
            if (v == null) return null;
            return (T) v;
        } catch (ClassCastException ce) {
            return null;
        }
    }

    public <T> T get(String key, T defaultValue) {
        T rtn = get(key);
        if (rtn == null) {
            rtn = defaultValue;
        }
        return rtn;
    }

    void set(String key, Object value) {
        if (threadSession.get() == null) threadSession.set(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));
        if (value == null) {
            remove(key);
        } else {
            threadSession.get().put(key, value);
        }
    }

    public void put(String key, ArrayList value) {
        if (value == null || value.size() == 0) {
            remove(key);
        } else {
            Object val = value.get(0);
            if (val instanceof String || val instanceof Integer || val instanceof Boolean || val instanceof Long || val instanceof Float) {
                set(key, value.toArray());
            } else {
                throw new RuntimeException(String.format("Illegal Type Exception for SessionContext key [%s]", key));
            }
        }
    }

    public void put(String key, String value) {
        if (value == null || value.trim().equals("")) {
            remove(key);
        } else {
            set(key, value.trim());
        }
    }

    public void put(String key, String[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Long value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Long[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Integer value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Integer[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Float value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Float[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Boolean value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void remove(String key) {
        if (threadSession.get() == null) return;

        threadSession.get().remove(key);
    }

    public void flush() {
        SessionCache cache = _cache.get(properties.get("session.cache.type"));
        cache.flush();
    }
}
package com.heartzones.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class Request {
    private final static Logger logger = LoggerFactory.getLogger(Request.class);

    static final ThreadLocal<Map<String, Object>> threadRequest = new ThreadLocal<>();
    static final ThreadLocal<String> threadRequestId = new ThreadLocal<>();

    public static class Constants {
        public final static String cacheDelete = "cacheDelete";
        public final static String cacheStore = "cacheStore";
        public final static String cookieDelete = "cookieDelete";
        public final static String Origin = "origin";
        public final static String Host = "host";
        public final static String URI = "URI";
        public final static String Domain = "domain";
        public final static String writeSessionOut = "write_session";
    }

    String id() {
        return threadRequestId.get();
    }
    String id(String value) {
        if (value == null) value = UUID.randomUUID().toString();

        threadRequestId.set(value);
        if (threadRequest.get() == null) threadRequest.set(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));

        MDC.put("requestId", value);

        return id();
    }
    void init() {
        threadRequest.remove();
        threadRequestId.remove();
        id(null);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        try {
            if (threadRequest.get() == null) threadRequest.set(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));
            Object v = threadRequest.get().get(key);
            if (v == null) return null;
            return (T) v;
        } catch (ClassCastException ce) {
            return null;
        }
    }

    public <T> T get(String key, T defaultValue) {
        T rtn = get(key);
        if (rtn == null) {
            rtn = defaultValue;
        }
        return rtn;
    }

    void set(String key, Object value) {
        if (value == null) {
            remove(key);
        } else {
            if (threadRequest.get() == null) threadRequest.set(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));
            threadRequest.get().put(key, value);
        }
    }

    public void put(String key, ArrayList value) {
        if (value == null || value.size() == 0) {
            remove(key);
        } else {
            Object val = value.get(0);
            if (val instanceof String || val instanceof Integer || val instanceof Boolean || val instanceof Long || val instanceof Float || val instanceof Byte) {
                set(key, value.toArray());
            } else {
                throw new RuntimeException(String.format("Illegal Type Exception for SessionContext key [%s]", key));
            }
        }
    }

    public void put(String key, String value) {
        if (value == null || value.trim().equals("")) {
            remove(key);
        } else {
            set(key, value.trim());
        }
    }

    public void put(String key, String[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Long value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Long[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Integer value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Integer[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Float value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Float[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void put(String key, Boolean value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

    public void remove(String key) {
        if (threadRequest.get() == null) return;
        threadRequest.get().remove(key);
    }

    public void put(String key, byte[] value) {
        if (value == null) {
            remove(key);
        } else {
            set(key, value);
        }
    }

}

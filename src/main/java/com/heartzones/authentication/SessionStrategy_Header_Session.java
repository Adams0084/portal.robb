package com.heartzones.authentication;

import com.heartzones.ApplicationProperties;

import java.util.Map;

class SessionStrategy_Header_Session implements SessionStrategy {
    @Override
    public Boolean load(SessionContext sessionContext, ApplicationProperties properties, Map<String, String> data) {
        String Id = SessionContext.value(data, properties.get("session.header.name"));
        if (Id == null) return null;
        Id= Id.trim();

        sessionContext.load(Id);

        return true;
    }
}

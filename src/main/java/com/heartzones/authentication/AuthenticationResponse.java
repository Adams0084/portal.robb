package com.heartzones.authentication;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationResponse implements ContainerResponseFilter {
    private final static Logger logger = LoggerFactory.getLogger(AuthenticationRequest.class);

    @Context
    private ResourceInfo info;

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @Override
    public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {
        try {
            if (session.sessionId() != null) {
                Integer expires = Integer.parseInt(properties.get("session.timeout")) * 60;

                String domain = properties.get("session.cookie.domain", session.Request.get(Request.Constants.Domain));
                NewCookie c;
                if (domain.toLowerCase().contains("localhost") || domain.equals("")) {
                    c = NewCookie.valueOf(String.format("%s=%s;Path=/;Max-Age=%d", properties.get("session.cookie.name"), session.sessionId(), expires));
                } else {
                    // If not an IP address add the . at the start
                    Pattern pattern = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
                    Matcher matcher = pattern.matcher(domain);
                    if (!matcher.matches())
                        domain = "." + domain;

                    c = NewCookie.valueOf(String.format("%s=%s;Domain=%s;Path=/;Max-Age=%d", properties.get("session.cookie.name"), session.sessionId(), domain, expires));
                }
                addCookieToResponse(response, c);
            }

            if (session.Request.get(Request.Constants.cookieDelete, false)) {
                addCookieToResponse(response, null);
            }

            Method method = info.getResourceMethod();
            if (method != null) {
                Authentication auth = method.getAnnotation(Authentication.class);
                if (auth != null) {
                    if (!auth.sessionReset()) {
                        session.Request.put(Request.Constants.cacheStore, false);
                    }
                }
            }
            Map<String, Object> headers = new HashMap<>();

            //Add Browser Cache Headers
            Boolean browserCacheVal = null;
            if (method != null) {
                BrowserCache browserCache = method.getAnnotation(BrowserCache.class);
                browserCacheVal = browserCache == null ? null : browserCache.value();
            }

            if (browserCacheVal == null || !browserCacheVal) {
                headers.put("Cache-Control", "no-cache, no-store, must-revalidate");
                headers.put("Pragma", "no-cache");
                headers.put("Expires", 9L);
            }

            for (String k : headers.keySet()) {
                response.getHeaders().add(k, headers.get(k));
            }

            //SessionContext Management
            if (session.sessionId() != null) {
                //Add User Headers
                headers.put(properties.get("session.header.name"), session.sessionId());

                if (session.Request.get(Request.Constants.cacheDelete, false))
                    session.sessionContext.delete();
                else if (session.Request.get(Request.Constants.cacheStore, true))
                    session.sessionContext.save();
                if (session.Request.get(Request.Constants.writeSessionOut, false) && response.getStatus() == 200) {
                    Gson gson = new Gson();
                    response.setEntity(gson.toJson(session.publicData(properties)));
                }
            }
        } finally {
            session.init();
        }
    }

    private static void addCookieToResponse(ContainerResponseContext response, NewCookie cookie) {
        boolean found = false;
        if (response.getHeaders().containsKey(HttpHeaders.SET_COOKIE)) {
            ListIterator<Object> cookies = response.getHeaders().get(HttpHeaders.SET_COOKIE).listIterator();
            while (cookies.hasNext()) {
                String cookieBody = cookies.next().toString();
                if (cookieBody.startsWith(cookie.getName())) {
                    found = true;
                    cookies.set(cookie);
                }
            }
        }
        if (!found) {
            response.getHeaders().add(HttpHeaders.SET_COOKIE, cookie);
        }
    }
}

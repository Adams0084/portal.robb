package com.heartzones.authentication;

import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ext.Provider;
import java.util.function.Supplier;

@Provider
public class SessionFactory implements Supplier<Session> {
    private static final Logger logger = LoggerFactory.getLogger(SessionFactory.class);

    private final Session session;
    public static ApplicationProperties properties;

    public SessionFactory() {
        session = new Session().initialize(properties);
    }

    @Override
    public Session get() {
        return session;
    }
}

package com.heartzones.authentication;

import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationRequest implements ContainerRequestFilter {
    private final static Logger logger = LoggerFactory.getLogger(AuthenticationRequest.class);

    @Context
    ResourceInfo info;

    @Context
    ApplicationProperties properties;

    @Context
    Session session;

    @Context
    private HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext request) throws IOException {
        try {
            session.init();

            servletRequest.setAttribute("request.id", session.requestId());

            session.Request.put(Request.Constants.URI, request.getUriInfo().getAbsolutePath().toString());
            session.Request.put(Request.Constants.Host, request.getUriInfo().getAbsolutePath().getScheme() + "://" + request.getUriInfo().getAbsolutePath().getAuthority());
            session.Request.put(Request.Constants.Domain, request.getUriInfo().getAbsolutePath().getAuthority());

            if (Boolean.parseBoolean(properties.get("server.https.enforce","false"))) {
                session.Request.put(Request.Constants.URI, ((String)session.Request.get(Request.Constants.URI)).replace("http://", "https://"));
                session.Request.put(Request.Constants.Host, ((String)session.Request.get(Request.Constants.Host)).replace("http://", "https://"));
            }

            logger.info("URI " + session.Request.get(Request.Constants.URI));

            /* evaluate CORS functionality */
            session.Request.put(Request.Constants.Origin, request.getHeaders().getFirst("Origin"));

            /* evaluate the method type */
            if (request.getMethod().equalsIgnoreCase("OPTIONS") || request.getMethod().equalsIgnoreCase("HEAD")) {
                logger.info(String.format("%s request method: %s", request.getMethod().toUpperCase(), session.Request.get(Request.Constants.Origin, "")));

                session.Request.put(Request.Constants.cacheStore, false);

                request.abortWith(Response.status(Response.Status.OK).entity("").build());
                return;
            }

            /* evaluate SessionContext */
            MultivaluedMap<String, String> rHeaders = request.getHeaders();
            Map<String, String> headers = new HashMap<>();
            for (String k : rHeaders.keySet()) {
                headers.put(k, rHeaders.getFirst(k));
            }

            Map<String, Cookie> rCookies = request.getCookies();
            Map<String, String> cookies = new HashMap<>();
            for (String cName : rCookies.keySet()) {
                cookies.put(cName, rCookies.get(cName).getValue());
            }

            headers.putAll(cookies);
            session.sessionContext.load(headers);

            /* evaluate Role Authorization */
            Method resourceMethod = null;
            if (info != null) {
                resourceMethod = info.getResourceMethod();
            }

            if (checkMethod (session, resourceMethod) == AuthenticationCheck.Failed) {
                logger.info("Failed method check: " + request.getUriInfo().getAbsolutePath().toString());
                request.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            }
        } catch (Exception ex) {
            throw new RuntimeException("Authentication Exception", ex);
        }
    }

    public AuthenticationCheck checkMethod(Session session, Method method) {
        if (method == null) {
            return AuthenticationCheck.NotValidated;
        }

        Authentication auth = method.getAnnotation(Authentication.class);
        if (auth == null) {
            //get Auth Annotation from class if Method Annotation doesn't exist
            auth = method.getDeclaringClass().getAnnotation(Authentication.class);

            if (auth == null) {
                logger.info("Authentication - Resource not annotated : " + method.getDeclaringClass().getCanonicalName() + "." + method.getName());
                return AuthenticationCheck.NotValidated;
            }
        }

        if (check_roles(session, auth.value())) {
            return AuthenticationCheck.Passed;
        } else {
            return AuthenticationCheck.Failed;
        }
    }

    private boolean check_roles(Session session, String role) {
        return check_roles(session, role.split(","));
    }

    private boolean check_roles(Session session, String[] roles) {
        return check_roles(session, roles, session.rolesList().toArray(new String[0]), session.isAuthenticated());
    }
    private boolean check_roles(Session session, String[] roles, String[] uRoles, Boolean isAuthenticated) {
        Set<String> cleanedRoles = new HashSet<>();
        for (String role : roles) {
            cleanedRoles.add(role);
        }

        Set<String> userRoles = new HashSet<>();
        for (String role : uRoles) {
            userRoles.add(role);
        }

        logger.info(String.format("check roles: %s in %s", Arrays.toString(roles), Arrays.toString(userRoles.toArray())));

        if (cleanedRoles.contains("disabled")) return false;
        if (cleanedRoles.contains("none")) return true;

        if (cleanedRoles.contains("all") || cleanedRoles.contains("*")) {
            if (session.isAuthenticated()) return true;
        }

        if (!isAuthenticated) return false;

        for (String role : cleanedRoles) {
            for (String ur : userRoles) {
                if (ur.equalsIgnoreCase(role)) return true;
            }
        }

        return false;
    }

}
package com.heartzones.authentication;


public enum AuthenticationCheck {
    NotValidated,
    Passed,
    Failed;

    public boolean passed() {
        return this == Passed;
    }

    public boolean isValidated() {
        return this != NotValidated;
    }
}

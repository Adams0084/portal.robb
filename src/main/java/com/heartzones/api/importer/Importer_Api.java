package com.heartzones.api.importer;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Constants;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.FileUploadDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.FileUpload;
import com.heartzones.models.User;
import com.heartzones.processor.FileProcessor;
import com.heartzones.store.FileManager;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Path("/data")
public class Importer_Api {
    private final static Logger logger = LoggerFactory.getLogger(Importer_Api.class);

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    public Importer_Api() {
    }

    public Importer_Api(ApplicationProperties properties, Session session) {
        this.properties = properties;
        this.session = session;
    }

    @GET
    @Path("info/{dataType}")
    @Authentication("teacher,owner")
    public Response file_info_get(@PathParam("dataType") String dataType) {
        Set<Parameter> parameters = new LinkedHashSet<>();
        parameters.add(new Parameter("account", Operator.EQUALS, session.get(Constants.accountCurrent)));
        parameters.add(new Parameter("file_type", Operator.EQUALS, dataType));
        parameters.add(new Parameter("status", Operator.EQUALS, "static"));
        parameters.add(new Parameter("create_user", Operator.EQUALS, session.userId()));

        FileUploadDataManager fileUploadDataManager = new FileUploadDataManager(properties);
        FileUpload existingFile = fileUploadDataManager.get(session.userId(), parameters);

        if (existingFile == null || Strings.isNullOrEmpty(existingFile.getFile_name()))
            return Response.status(404).build();

        File file = new File(existingFile.getFile_name());

        if (!file.exists()) return Response.status(404).build();

        logger.info(String.format("Getting File %s: %s[%d] for %s", dataType, existingFile.getFile_name(), file.length(), session.userName()));

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String type = fileNameMap.getContentTypeFor(existingFile.getFile_type());

        if (Strings.isNullOrEmpty(type))
            type = MediaType.APPLICATION_OCTET_STREAM;

        Map<String, Object> rtn = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        rtn.put("updated", DataTools.Dates.formatISODateTime(existingFile.getUpdated()));
        rtn.put("update_user", session.get(Constants.displayName));

        rtn.put("type", type);
        rtn.put("name", existingFile.getFile_type());
        rtn.put("created", DataTools.Dates.formatISODateTime(existingFile.getCreated()));
        rtn.put("create_user", session.get(Constants.displayName));

        Gson gson = new Gson();
        return Response.ok(file).type(MediaType.APPLICATION_JSON_TYPE).entity(gson.toJson(rtn)).build();
    }

    @GET
    @Path("{dataType}")
    @Authentication("teacher,owner")
    public Response file_get(@PathParam("dataType") String dataType) {
        Set<Parameter> parameters = new LinkedHashSet<>();
        parameters.add(new Parameter("account", Operator.EQUALS, session.get(Constants.accountCurrent)));
        parameters.add(new Parameter("file_type", Operator.EQUALS, dataType));
        parameters.add(new Parameter("status", Operator.EQUALS, "static"));

        FileUploadDataManager fileUploadDataManager = new FileUploadDataManager(properties);
        FileUpload existingFile = fileUploadDataManager.get(session.userId(), parameters);

        if (existingFile == null || Strings.isNullOrEmpty(existingFile.getFile_name())) return Response.status(404).build();

        String fileName = existingFile.getFile_name();

        if (properties.get("file.store.type").equalsIgnoreCase("file")) {
            File file = new File(fileName);

            if (file.exists()) {
                FileNameMap fileNameMap = URLConnection.getFileNameMap();
                String type = fileNameMap.getContentTypeFor(existingFile.getFile_type());

                if (Strings.isNullOrEmpty(type))
                    type = MediaType.APPLICATION_OCTET_STREAM;

                return Response.ok(file).type(type).build();
            }
        } else {
            throw new RuntimeException("unknown storage configuration [file.store.type]");
        }

        // return 404 if here because no response has been created yet
        return Response.status(404).build();
    }

    @POST
    @Path("{dataType}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Authentication("teacher,owner")
    public Response file_post(
            @PathParam("dataType") String dataType,
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {

        Set<Parameter> parameters = new LinkedHashSet<>();
        parameters.add(new Parameter("account", Operator.EQUALS, session.get(Constants.accountCurrent)));
        parameters.add(new Parameter("file_type", Operator.EQUALS, dataType));

        FileUploadDataManager fileUploadDataManager = new FileUploadDataManager(properties);
        FileUpload existingFileUpload = fileUploadDataManager.get(session.userId(), parameters);

        FileUpload fileUpload;
        if (existingFileUpload != null) {
            fileUpload = existingFileUpload;
        } else {
            fileUpload = new FileUpload(session.get(Constants.accountCurrent));
            fileUpload.setFile_type(dataType);
        }

        String fileName;
        if (properties.get("file.store.type").equalsIgnoreCase("file")) {
            FileManager fileManager = new FileManager(properties, session);
            if (Strings.isNullOrEmpty(fileUpload.getFile_name()))
                fileName = fileManager.write(inputStream);
            else
                fileName = fileManager.write(fileUpload.getFile_name(), inputStream);
        } else {
            throw new RuntimeException("unknown storage configuration [file.store.type]");
        }

        fileUpload.setFile_name(fileName);
        if (Strings.isNullOrEmpty(fileUpload.getStatus()))
            fileUpload.setStatus("loaded");
        fileUploadDataManager.save(session.userId(), fileUpload);

        /* Queue file for processing */
        if (fileUpload.getStatus().equalsIgnoreCase("loaded"))
            FileProcessor.process(properties, fileUpload);

        String responseText;
        if (fileUpload.getStatus().equalsIgnoreCase("duplicate"))
            responseText = "{\"message\":\"Duplicate file upload detected.\"}";
        else
            responseText = "{\"message\":\"File processing in queue.\"}";

        return Response.ok().entity(responseText).build();
    }
}

package com.heartzones.api.profile;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Path("/")
public class Profile_Api {
    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @GET
    @Path("profile")
    @Authentication("all")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response get() {
        return  get(session.userId());
    }

    @GET
    @Path("profile/{id}")
    @Authentication("system")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {
        UserDataManager userDataManager = new UserDataManager(properties);

        if (id != session.userId()) {
            if (!session.roleCheck("system"))
                return Response.status(401).build();
        }

        User u = userDataManager.get(id, new Parameter("id", Operator.EQUALS, id));

        if (u == null)
            return Response.status(401).build();

        Map<String, Object> rtn = DataTools.Objects.from(u);

        /* remove fields not intended for ui */
        rtn.remove("password");

        Gson gson = new Gson();
        return Response.ok().entity(gson.toJson(rtn)).build();
    }

    @POST
    @Path("profile")
    @Authentication("all")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request) {
        UserDataManager userDataManager = new UserDataManager(properties);

        String body;
        try {
            body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Gson gson = new Gson();
        Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        data = gson.fromJson(body, data.getClass());

        Integer id = ((Double) data.get("id")).intValue();

        if (!session.userId().equals(id)) {
            if (!session.roleCheck("system"))
                return Response.status(401).build();
        }

        User u = userDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, id));

        if (u == null)
            return Response.status(401).build();

        //remove fields that should not be set
        if (data.containsKey("id"))
            data.remove("id");
        if (data.containsKey("created"))
            data.remove("created");
        if (data.containsKey("create_user"))
            data.remove("create_user");
        if (data.containsKey("updated"))
            data.remove("updated");
        if (data.containsKey("update_user"))
            data.remove("update_user");

        if (data.containsKey("dob"))
            data.put("dob", ((String)data.get("dob")).substring(0, 10));

        //remove fields that remained the same
        Map<String, Object> currentData = DataTools.Objects.from(u);

        for (String k : currentData.keySet()) {
            if (data.containsKey(k)) {
                if (data.get(k) != null && data.get(k).equals(currentData.get(k)))
                    data.remove(k);
                else if (data.get(k) == null && currentData.get(k) == null)
                    data.remove(k);
            }
        }

        userDataManager.save(session.userId(), id, data);

        return Response.ok().build();
    }
}

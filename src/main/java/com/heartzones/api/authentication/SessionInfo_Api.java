package com.heartzones.api.authentication;

import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Request;
import com.heartzones.authentication.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class SessionInfo_Api {
    private final static Logger logger = LoggerFactory.getLogger(SessionInfo_Api.class);

    @Context
    ApplicationProperties properties;

    @Context
    Session session;

    public SessionInfo_Api() {
    }

    public SessionInfo_Api(ApplicationProperties properties, Session session) {
        this.properties = properties;
        this.session = session;
    }

    @GET
    @Path("auth/session")
    @Produces(MediaType.APPLICATION_JSON)
    @Authentication(value = "none", sessionReset = false)
    public Response sessionInfo() {
        session.Request.put(Request.Constants.writeSessionOut, true);
        return Response.ok().build();
    }

    @GET
    @Path("auth/session/refresh")
    @Produces(MediaType.APPLICATION_JSON)
    @Authentication(value = "none")
    public Response sessionInfoRefresh() {
        return sessionInfo();
    }
}

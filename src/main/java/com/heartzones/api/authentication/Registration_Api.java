package com.heartzones.api.authentication;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Request;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountDataManager;
import com.heartzones.dataManagers.AccountLicenseDataManager;
import com.heartzones.dataManagers.AccountUserDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.*;
import com.heartzones.processor.EmailProcessor;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

@Path("/")
public class Registration_Api {

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @POST
    @Path("auth/registration")
    @Authentication("none")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response post(@Context HttpServletRequest request) {
        try {
            String body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
            Gson gson = new Gson();
            Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            data = gson.fromJson(body, data.getClass());

            UserDataManager userDataManager = new UserDataManager(properties);
            User u = userDataManager.get(1, new Parameter("username", Operator.EQUALS, data.get("username")));
            if (u != null) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }

            u = new User();

            u.setUsername((String) data.get("username"));
            u.setFirst((String) data.get("first"));
            u.setLast((String) data.get("last"));
            u.setPhone((String) data.get("phone"));

            u.setAddress_1((String) data.get("address.line1"));
            u.setAddress_2((String) data.get("address.line2"));
            u.setCity((String) data.get("address.city"));
            u.setState((String) data.get("address.state"));
            u.setZip((String) data.get("address.zip"));

            u.getRoles().add("instructor");
            u.setOptional_info((Boolean) data.get("optional-info"));
            data.get("terms-agree");

            userDataManager.save(1, u);

            //save password since it is not part of the object
            Map<String,Object> passwordValue = new HashMap<>();
            passwordValue.put("password", data.get("password"));
            userDataManager.save(1, u.getId(), passwordValue);

            Account account = new Account();

            if ((Boolean) data.get("registration.business")) {
                account.setType(AccountTypes.Business.name());
            }
            if ((Boolean) data.get("registration.education")) {
                account.setType(AccountTypes.Educational.name());

                data.get("school.district");
            }
            if ((Boolean) data.get("registration.individual")) {
                account.setType(AccountTypes.Individual.name());
            }

            if (data.containsKey("name"))
                account.setName((String) data.get("name"));
            else
                account.setName(u.getLast() + "." + u.getFirst());

            account.setCity((String) data.get("address.city"));
            account.setAddress_1((String) data.get("address.line1"));
            account.setAddress_2((String) data.get("address.line2"));
            account.setState((String) data.get("address.state"));
            account.setZip((String) data.get("address.zip"));
            account.setUid(UUID.randomUUID());

            AccountDataManager accountDataManager = new AccountDataManager(properties);
            accountDataManager.save(1, account);

            AccountUser accountAdminUser = new AccountUser(account.getId());
            accountAdminUser.setUser(u.getId());
            accountAdminUser.setRole("admin");

            AccountUser accountOwnerUser = new AccountUser(account.getId());
            accountOwnerUser.setUser(u.getId());
            accountOwnerUser.setRole("owner");

            AccountUser accountTeacherUser = new AccountUser(account.getId());
            accountTeacherUser.setUser(u.getId());
            accountTeacherUser.setRole("teacher");

            AccountUserDataManager accountUserDataManager = new AccountUserDataManager(properties);
            accountUserDataManager.save(1, accountAdminUser);
            accountUserDataManager.save(1, accountOwnerUser);
            accountUserDataManager.save(1, accountTeacherUser);

            AccountLicense accountLicense = new AccountLicense(account.getId());
            accountLicense.setExpires(DataTools.Dates.addDays(new Date(), Integer.parseInt(properties.get("account.license.temp.days"))));
            accountLicense.setName(properties.get("account.license.temp.name"));
            accountLicense.setFeatures(properties.get("account.license.temp.features"));

            AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);
            accountLicenseDataManager.save(1, accountLicense);

            Login_Api login = new Login_Api(properties, session);
            login.sessionCreate(u, account);

            /* send welcome message */
            Email email = new Email();

            email.setSender(properties.get("email.sender.account.welcome", properties.get("email.sender")));
            email.setRecipiant(u.getUsername());
            email.setSubject(properties.get("email.subject.account.welcome"));

            String rtn = new BufferedReader(new InputStreamReader(ResetPassword_Api.class.getClassLoader().getResourceAsStream("messages/welcome.account.htm"))).lines().collect(Collectors.joining());

            rtn = rtn.replace("{{user.name}}", u.getDisplay());
            rtn = rtn.replace("{{account.name}}", account.getName());
            rtn = rtn.replace("{{host}}", session.Request.get(Request.Constants.Host));

            email.setBody(rtn);

            EmailProcessor.process(properties, email);

            SessionInfo_Api sessionInfo = new SessionInfo_Api(properties, session);
            return sessionInfo.sessionInfo();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

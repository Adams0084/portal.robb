package com.heartzones.api.authentication;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Constants;
import com.heartzones.authentication.Request;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.Account;
import com.heartzones.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

@Path("/")
public class Login_Api {
    private final static Logger logger = LoggerFactory.getLogger(Login_Api.class);

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    public Login_Api() {
    }

    public Login_Api(ApplicationProperties properties, Session session) {
        this.properties = properties;
        this.session = session;
    }

    @POST
    @Path("auth/login")
    @Authentication("none")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response post(@Context HttpServletRequest request) {
        try {
            String body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
            Gson gson = new Gson();
            Map<String, String> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            data = gson.fromJson(body, data.getClass());

            UserDataManager userDataManager = new UserDataManager(properties);

            logger.info(String.format("Login attempt for [%s]", data.get("username") == null ? "null" : data.get("username")));

            Set<Parameter> parameters = new LinkedHashSet<>();
            parameters.add(new Parameter("username", Operator.EQUALS, data.get("username")));
            parameters.add(new Parameter("password", Operator.EQUALS, data.get("password")));

            User u = userDataManager.get(1, parameters);
            if (u == null) {
                logger.info("401: Login attempt Failed.");
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }

            Date loggedIn = new Date();
            userDataManager.save(u.getId(), u.getId(), new HashMap<String, Object>() {{
                put("last_logged_in", loggedIn);
            }});
            u.setLast_logged_in(loggedIn);

            Account account = getCurrentAccount(u);

            sessionCreate(u, account);

            session.Request.put(Request.Constants.writeSessionOut, true);

            return Response.ok().build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Account getCurrentAccount(User u) {
        AccountDataManager accountDataManager = new AccountDataManager(properties);

        //remove account reference
        u.getPreferences().remove(Constants.accountCurrent);

        Account account = accountDataManager.get(u.getId(), new HashSet<>());

        if (account != null) {
            u.setRoles(Arrays.asList(account.getRoles().split(",")));

            return account;
        }
        return null;
    }

    @GET
    @Path("auth/logout")
    @Authentication("none")
    public Response logout() {
        session.Request.put(Request.Constants.cacheDelete, true);

        return Response.ok().build();
    }

    public void sessionCreate(User u, Account account) {
        session.put(Constants.userId, u.getId());
        session.put(Constants.authenticated, true);

        //System User Can't have accounts
        if (u.getAdmin()) {
            u.setRoles(new ArrayList<String>() {{
                add("system");
            }});
        }
        else if (account != null) {
            session.put(Constants.accountCurrent, account.getId());
            session.put(Constants.accountCurrentName, account.getName());
            session.put(Constants.accountCurrentLincse, account.getLicenses());
            session.put(Constants.accountCurrentLincseExpires, DataTools.Dates.formatISODateTime(account.getExpires()));
        }

        session.put(Constants.roles, Arrays.copyOf(u.getRoles().toArray(), u.getRoles().size(), String[].class));
        session.put(Constants.userName, u.getUsername());
        session.put(Constants.firstName, u.getFirst());
        session.put(Constants.lastName, u.getLast());
        session.put(Constants.displayName, u.getDisplay());
        session.put(Constants.sessionStart, DataTools.Dates.formatISODateTime(u.getLast_logged_in()));
        session.put(Constants.verified, DataTools.Dates.formatISODateTime(u.getVerified()));
    }
}

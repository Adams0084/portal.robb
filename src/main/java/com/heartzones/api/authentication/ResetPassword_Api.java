package com.heartzones.api.authentication;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Request;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.MagicLinkDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.Email;
import com.heartzones.models.MagicLink;
import com.heartzones.models.User;
import com.heartzones.processor.EmailProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

@Path("/")
public class ResetPassword_Api {
    private final static Logger logger = LoggerFactory.getLogger(ResetPassword_Api.class);

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @GET
    @Path("auth/reset-password/{username}")
    @Authentication("none")
    public Response get(@PathParam("username") String username) {
        if (Strings.isNullOrEmpty(username))
            return Response.status(400).entity("Validation Error: username missing.").build();

        UserDataManager userDataManager = new UserDataManager(properties);
        MagicLinkDataManager magicLinkDataManager = new MagicLinkDataManager(properties);

        User u = userDataManager.get(1, new Parameter("username", Operator.EQUALS, username));

        if (u == null)
            return Response.ok().build();

        if (!Strings.isNullOrEmpty(u.getUsername())) {
            MagicLink magic = new MagicLink();
            magic.setAction("ResetPassword_Api");
            magic.setLink(UUID.randomUUID());
            magic.getData().put("user.id", u.getId());
            magic.setExpires(DataTools.Dates.addDays(new Date(), 3));

            magicLinkDataManager.save(1, magic);

            String link = String.format("%s/login/reset-password.html?%s", session.Request.get(Request.Constants.Host), magic.getLink().toString());

            logger.info(String.format("Request: %s", link));

            Email email = new Email();

            email.setSender(properties.get("email.sender.reset.password", properties.get("email.sender")));
            email.setRecipiant(u.getUsername());
            email.setTemplate(properties.get("email.sendgrid.template.password.reset", null));
            email.setSubject(properties.get("email.subject.password.reset"));


            String rtn = new BufferedReader(new InputStreamReader(ResetPassword_Api.class.getClassLoader().getResourceAsStream("messages/reset.password.htm"))).lines().collect(Collectors.joining());

            rtn = rtn.replace("{{link}}", link);
            rtn = rtn.replace("{{host}}", session.Request.get(Request.Constants.Host));
            email.setBody(rtn);

            EmailProcessor.process(properties, email);
        }
        return Response.ok().build();
    }

    /*
        FormData should be url encoded to allow special characters to be passed.
    */
    @POST
    @Path("auth/reset-password")
    @Consumes(MediaType.APPLICATION_JSON)
    @Authentication("none")
    public Response post(@Context HttpServletRequest request) {
        String body = null;
        try {
            body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
            Gson gson = new Gson();
            Map<String, String> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            data = gson.fromJson(body, data.getClass());

            UserDataManager userDataManager = new UserDataManager(properties);
            MagicLinkDataManager magicLinkDataManager = new MagicLinkDataManager(properties);

            MagicLink magic = magicLinkDataManager.get(1, new Parameter("link", Operator.EQUALS, UUID.fromString(data.get("link"))));

            if (magic == null || !magic.getAction().equalsIgnoreCase("ResetPassword_Api"))
                throw new RuntimeException("Invalid link for operation");

            String password = data.get("password").trim();

            ///TODO: Add Password Complexity Check

            if (Strings.isNullOrEmpty(password))
                throw new RuntimeException("Password value can not be empty");

            //JSON Deserializer seems to make this a Double
            User u = userDataManager.get(1, new Parameter ("id", Operator.EQUALS, ((Double)magic.getData().get("user.id")).intValue()));

            Map<String,Object> passwordValue = new HashMap<>();
            passwordValue.put("password", password);
            userDataManager.save(1, u.getId(), passwordValue);
            magic.setUsed(new Date());
            magicLinkDataManager.save(u.getId(), magic);

            logger.info("password reset successful");
            return Response.ok().build();
        } catch (Exception e) {
            logger.warn(String.format("failed to reset password with request body : %s", body));
            throw new RuntimeException(e);
        }
    }
}

package com.heartzones.api.authentication;

import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Constants;
import com.heartzones.authentication.Request;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.MagicLinkDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.Email;
import com.heartzones.models.MagicLink;
import com.heartzones.processor.EmailProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/")
public class Verify_Api {
    private final static Logger logger = LoggerFactory.getLogger(ResetPassword_Api.class);

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @Path("auth/verify")
    @GET()
    @Authentication("all")
    public Response get() {
        if (session.get(Constants.verified) != null)
            return Response.ok().build();

        MagicLink magic = new MagicLink();
        magic.setAction("VerifyAccount");

        magic.setLink(UUID.randomUUID());
        magic.getData().put("user.id", session.userId());
        magic.setExpires(DataTools.Dates.addDays(new Date(), 3));

        MagicLinkDataManager magicLinkDataManager = new MagicLinkDataManager(properties);
        magicLinkDataManager.save(session.userId(), magic);

        String link = String.format("%s/login/verify.html?%s", session.Request.get(Request.Constants.Host), magic.getLink().toString());

        logger.info(String.format("Verify_Api Link: %s", link));

        Email email = new Email();

        email.setSender(properties.get("email.sender.account.verify", properties.get("email.sender")));
        email.setRecipiant(session.get(Constants.userName));
        email.setTemplate(properties.get("email.sendgrid.template.account.verify", null));
        email.setSubject(properties.get("email.subject.account.verify"));

        String rtn = new BufferedReader(new InputStreamReader(ResetPassword_Api.class.getClassLoader().getResourceAsStream("messages/verify.account.htm"))).lines().collect(Collectors.joining());

        rtn = rtn.replace("{{link}}", link);
        rtn = rtn.replace("{{host}}", session.Request.get(Request.Constants.Host));
        email.setBody(rtn);

        EmailProcessor.process(properties, email);

        return Response.ok().build();
    }

    @POST
    @Path("auth/verify/{link}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Authentication("all")
    public Response post(@PathParam("link") UUID link, @Context HttpServletRequest request) {
        try {
            UserDataManager userDataManager = new UserDataManager(properties);
            MagicLinkDataManager magicLinkDataManager = new MagicLinkDataManager(properties);

            MagicLink magic = magicLinkDataManager.get(session.userId(), new Parameter("link", Operator.EQUALS, link));

            if (magic == null || !magic.getAction().equalsIgnoreCase("VerifyAccount"))
                throw new RuntimeException("Invalid link for operation");

            Integer linkUser = ((Double)magic.getData().get("user.id")).intValue();

            if (!linkUser.equals(session.userId()))
                return Response.status(Response.Status.UNAUTHORIZED).build();

            Date now = new Date();
            userDataManager.save(session.userId(), session.userId(), new HashMap<String, Object>() {{
                put("verified", now);
            }});

            session.put(Constants.verified, DataTools.Dates.formatISODateTime(now));

            magic.setUsed(new Date());
            magicLinkDataManager.save(session.userId(), magic);

            return Response.ok().build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

package com.heartzones.api.performance;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Session;
import com.heartzones.data.*;
import com.heartzones.models.SessionSummary;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.*;

@Path("/performance")
public class Performance_Api {

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @GET
    @Path("summary")
    @Authentication("all")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response summary(@Context UriInfo uriInfo) {
        Set<Parameter> params =  DataTools.Objects.mapQueryString(uriInfo.getQueryParameters(), new SessionSummary());

        ConnectionManager connectionManager = new ConnectionManager(properties);
        Connection connection = connectionManager.get();

        String where = DataTools.DB.whereBuilder(params);
        String statement = "SELECT data.* " +
                "FROM ( " +
                "SELECT CASE WHEN roles IS NULL THEN NULL ELSE account END as account, user " +
                "FROM ( " +
                "SELECT a.id as account, u.id as user, " +
                "coalesce(GROUP_CONCAT(au.role), IF(u.admin, 'system', NULL)) AS roles " +
                "FROM user AS u  " +
                "LEFT JOIN account AS a ON 1 = 1 " +
                "LEFT JOIN account_user AS au ON au.account = a.id AND au.user = u.id AND au.deleted = 0 AND au.role <> 'student' " +
                "JOIN account_license AS al ON al.account = a.id AND al.expires > now() AND al.deleted = 0 " +
                "WHERE u.id = ? AND u.deleted = 0 " +
                "GROUP BY u.id, a.id " +
                ") as a1 " +
                ") as access " +
                "JOIN ( " +
                "SELECT ss.id, ss.account, ss.file, ss.session, ss.account_roster, " +
                "ss.start, ss.end, ss.fit, ss.hrm, ss.hz_max, ss.hz_shr, ss.hz_t1, ss.hz_t2, ss.pwr, ss.sc, ss.sdm, ss.spd, " +
                "ss.step_max, ar.user, ar.id_local, ar.first, ar.last, ar.nick, ar.dob, ar.male, ar.height, ar.weight " +
                "FROM hz_portal.session_summary AS ss " +
                "JOIN account_roster AS ar ON ar.id = ss.account_roster " +
                ") as data ON data.account = access.account or data.user = access.user " +
                "{{WHERE}}";
        String myStatement = statement.replace("{{WHERE}}", where);

        Map<String, Object>  parameters_query = new LinkedHashMap<>();
        parameters_query.put("user_accounts", session.userId());

        for (Parameter p: params)
            parameters_query.put(p.getName(), p.getValue());

        List<Map<String, Object>> rtn = connection.executeQuery(myStatement, parameters_query);

        Encryption encryption = new Encryption(properties);
        for (Map<String, Object> row : rtn) {
            row.put("first", encryption.decrypt((String) row.get("first")));
            row.put("last", encryption.decrypt((String) row.get("last")));
            row.put("nick", encryption.decrypt((String) row.get("nick")));

            row.put("start", DataTools.Dates.formatISODateTime((Date) row.get("start")));
            row.put("end", DataTools.Dates.formatISODateTime((Date) row.get("end")));

            row.put("dob", DataTools.Dates.formatDate((Date) row.get("dob")));
        }

        Gson gson = new Gson();
        return Response.ok().entity(gson.toJson(rtn)).build();
    }
}

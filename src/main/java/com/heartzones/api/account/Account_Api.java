package com.heartzones.api.account;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.*;
import com.heartzones.models.Account;
import com.heartzones.models.AccountUser;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.Set;

@Path("/")
public class Account_Api {

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @GET
    @Path("account/{id}")
    @Authentication("system,admin")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {
        AccountDataManager accountDataManager = new AccountDataManager(properties);

        Account account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, id));
        if (account == null) return Response.status(401).build();

        Gson gson = new Gson();
        return Response.ok().entity(gson.toJson(account)).build();
    }

    @GET
    @Path("accounts")
    @Authentication("system,admin")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response list(@Context UriInfo uriInfo) {
        Set<Parameter> params =  DataTools.Objects.mapQueryString(uriInfo.getQueryParameters(), new Account());
        AccountDataManager accountDataManager = new AccountDataManager(properties);

        List<Account> rtn = accountDataManager.list(session.userId(), params);

        Gson gson = new Gson();
        return Response.ok().entity(gson.toJson(rtn)).build();
    }


    @GET
    @Path("account/delete/{id}")
    @Authentication("system")
    public Response remove(@PathParam("id") Integer id) {
        AccountDataManager accountDataManager = new AccountDataManager(properties);

        Account account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, id));
        if (account == null) return Response.status(Response.Status.NOT_FOUND).build();

        AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);
        AccountRosterDataManager accountRosterDataManager = new AccountRosterDataManager(properties);
        AccountUserDataManager accountUserDataManager = new AccountUserDataManager(properties);
        FileUploadDataManager fileUploadDataManager = new FileUploadDataManager(properties);

        List<AccountUser> users = accountUserDataManager.list(session.userId(), new Parameter("account", Operator.EQUALS, account.getId()));

        accountLicenseDataManager.delete(session.userId(), new Parameter("account", Operator.EQUALS, account.getId()));
        accountRosterDataManager.delete(session.userId(), new Parameter("account", Operator.EQUALS, account.getId()));
        accountUserDataManager.delete(session.userId(), new Parameter("account", Operator.EQUALS, account.getId()));
        fileUploadDataManager.delete(session.userId(), new Parameter("account", Operator.EQUALS, account.getId()));

        UserDataManager userDataManager = new UserDataManager(properties);
        for (AccountUser u: users)
            userDataManager.delete(session.userId(),  new Parameter("id", Operator.EQUALS, u.getUser()));

        accountDataManager.delete(session.userId(), new Parameter("id", Operator.EQUALS, id));

        return Response.ok().build();
    }
}

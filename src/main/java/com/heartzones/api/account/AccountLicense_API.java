package com.heartzones.api.account;

import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Authentication;
import com.heartzones.authentication.Session;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountDataManager;
import com.heartzones.dataManagers.AccountLicenseDataManager;
import com.heartzones.models.Account;
import com.heartzones.models.AccountTypes;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

@Path("/")
public class AccountLicense_API {

    @Context
    private ApplicationProperties properties;

    @Context
    private Session session;

    @POST
    @Path("account/license/extend")
    @Authentication("system")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response extend(@Context HttpServletRequest request) {
        String body;
        try {
            body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Gson gson = new Gson();
        Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        data = gson.fromJson(body, data.getClass());

        if (!data.containsKey("id")) return Response.status(400).build();

        Integer id = Integer.parseInt((String)data.get("id"));
        Integer days = Integer.parseInt(properties.get("account.license.temp.extension.days"));

        if (data.containsKey("days"))
            days = ((Double) data.get("days")).intValue();

        AccountDataManager accountDataManager = new AccountDataManager(properties);
        Account account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, id));
        if (account == null) return Response.status(401).build();

        AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);

        Set<Parameter> params = new LinkedHashSet<>();
        params.add(new Parameter("account", Operator.EQUALS, id));
        params.add(new Parameter("expires", Operator.GREATER_THAN, new Date()));
        params.add(new Parameter("name", Operator.EQUALS, properties.get("account.license.temp.name")));

        List<com.heartzones.models.AccountLicense> accountLicenses = accountLicenseDataManager.list(session.userId(), params);
        Boolean adjustedCurrentLicense = false;
        // extend current active temp license
        for (com.heartzones.models.AccountLicense al : accountLicenses) {
            Map<String, Object> saveData = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            saveData.put("expires", DataTools.Dates.addDays(al.getExpires(), days));
            accountLicenseDataManager.save(session.userId(), al.getId(), saveData);

            adjustedCurrentLicense = true;
            break;
        }

        if (!adjustedCurrentLicense) {
            // create new Temp license

            com.heartzones.models.AccountLicense al = new com.heartzones.models.AccountLicense(account.getId());
            al.setName(properties.get("account.license.temp.name"));
            al.setExpires(DataTools.Dates.addDays(new Date(), days));
            al.setFeatures(properties.get("account.license.temp.features"));

            accountLicenseDataManager.save(session.userId(), al);
        }

        account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, account.getId()));

        return Response.ok().entity(gson.toJson(account)).build();
    }
    @POST
    @Path("account/license/upgrade")
    @Authentication("system")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response upgrade(@Context HttpServletRequest request) {
        String body;
        try {
            body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Gson gson = new Gson();
        Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        data = gson.fromJson(body, data.getClass());

        if (!data.containsKey("id")) return Response.status(400).build();

        Integer id = Integer.parseInt((String)data.get("id"));

        AccountDataManager accountDataManager = new AccountDataManager(properties);
        Account account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, id));
        if (account == null) return Response.status(401).build();

        Date now = new Date();
        com.heartzones.models.AccountLicense accountLicense = new com.heartzones.models.AccountLicense(account.getId());
        if (account.getType().equalsIgnoreCase(AccountTypes.Educational.name()) )  {
            accountLicense.setName(properties.get("account.license.edu.name"));
            accountLicense.setExpires(DataTools.Dates.addDays(now, Integer.parseInt(properties.get("account.license.edu.days"))));
            accountLicense.setFeatures(properties.get("account.license.edu.features"));
        }
        else if (account.getType().equalsIgnoreCase(AccountTypes.Business.name()) )  {
            accountLicense.setName(properties.get("account.license.bus.name"));
            accountLicense.setExpires(DataTools.Dates.addDays(now, Integer.parseInt(properties.get("account.license.bus.days"))));
            accountLicense.setFeatures(properties.get("account.license.bus.features"));
        }
        else if (account.getType().equalsIgnoreCase(AccountTypes.Individual.name()) )  {
            accountLicense.setName(properties.get("account.license.ind.name"));
            accountLicense.setExpires(DataTools.Dates.addDays(now, Integer.parseInt(properties.get("account.license.ind.days"))));
            accountLicense.setFeatures(properties.get("account.license.ind.features"));
        }
        else {
            throw new RuntimeException(String.format("Unknown Account Type [%s] on [%d]", account.getType() == null ? "null" : account.getType(), account.getId()));
        }

        AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);

        Set<Parameter> params = new HashSet<>();
        params.add(new Parameter("account", Operator.EQUALS, account.getId()));
        params.add(new Parameter("expires", Operator.GREATER_THAN, now));
        params.add(new Parameter("name", Operator.EQUALS, properties.get("account.license.temp.name")));

        List<com.heartzones.models.AccountLicense> accountLicenses = accountLicenseDataManager.list(session.userId(), params);
        for (com.heartzones.models.AccountLicense al: accountLicenses) {
            al.setExpires(now);
            accountLicenseDataManager.save(session.userId(), al);
        }

        accountLicenseDataManager.save(session.userId(), accountLicense);

        account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, account.getId()));

        return Response.ok().entity(gson.toJson(account)).build();
    }

    @POST
    @Path("account/license/remove")
    @Authentication("system")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response remove(@Context HttpServletRequest request) {
        String body;
        try {
            body = new BufferedReader(new InputStreamReader(request.getInputStream())).lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Gson gson = new Gson();
        Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        data = gson.fromJson(body, data.getClass());

        if (!data.containsKey("id")) return Response.status(400).build();

        Integer id = Integer.parseInt((String) data.get("id"));

        AccountDataManager accountDataManager = new AccountDataManager(properties);
        Account account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, id));
        if (account == null) return Response.status(401).build();

        AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);

        Set<Parameter> params = new LinkedHashSet<>();
        params.add(new Parameter("account", Operator.EQUALS, id));
        params.add(new Parameter("expires", Operator.GREATER_THAN, new Date()));

        List<com.heartzones.models.AccountLicense> accountLicenses = accountLicenseDataManager.list(session.userId(), params);

        // extend current active temp license
        for (com.heartzones.models.AccountLicense al : accountLicenses) {
            Map<String, Object> saveData = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            saveData.put("expires", new Date());
            accountLicenseDataManager.save(session.userId(), al.getId(), saveData);
        }
        account = accountDataManager.get(session.userId(), new Parameter("id", Operator.EQUALS, account.getId()));

        return Response.ok().entity(gson.toJson(account)).build();
    }
}

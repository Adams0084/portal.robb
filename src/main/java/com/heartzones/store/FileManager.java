package com.heartzones.store;

import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Session;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class FileManager {
    private ApplicationProperties properties;

    public FileManager(ApplicationProperties properties, Session session) {
        this.properties = properties;
        Session session1 = session;
    }

    public InputStream read(String fileName) {

        try {
            File file = new File(fileName);
            if (!file.exists()) return null;

            return new FileInputStream(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String write(InputStream inputStream) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

        String fileName = String.format("%s/%s/%s", properties.get("file.store.location"), dateFormat.format(new Date()), UUID.randomUUID().toString());
        return write(fileName, inputStream);
    }

    public String write(String fileName, InputStream inputStream) {

        try {
            int read = 0;
            byte[] bytes = new byte[1024];

            File outFile = new File(fileName);
            outFile.getParentFile().mkdirs();
            OutputStream out = new FileOutputStream(outFile);
            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();

            return fileName;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

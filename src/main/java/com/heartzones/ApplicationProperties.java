package com.heartzones;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ApplicationProperties {
    private static final Logger logger = LogManager.getLogger(ApplicationProperties.class);

    private static final String resourceFileName = "/application.properties";
    private static final String localPropertiesFileName = "src/main/resources/my.properties";

    private static Map<String, String> properties = new HashMap<>();

    private Class loaderClass;

    public ApplicationProperties() {
        loaderClass = ApplicationProperties.class;
    }
    public ApplicationProperties(Class loaderClass) {
        this();
        this.loaderClass = loaderClass;
    }

    public void initialize() {
        InputStream resourceFile = loaderClass.getResourceAsStream(resourceFileName);
        load(resourceFile);

        load(new File(localPropertiesFileName));

        loadVersionInfo();
    }

    void loadVersionInfo() {
        Gson gson = new Gson();
        try {
            InputStream version = loaderClass.getResourceAsStream("/version.json");
            Map<String, String> versionInfo = gson.fromJson(new BufferedReader(new InputStreamReader(version, "UTF-8")), new TypeToken<Map<String, String>>() {
            }.getType());

            for (String key: versionInfo.keySet()) {
                properties.put("version." + key, versionInfo.get(key));
                logger.info(String.format("version.%s: '%s'", key, properties.get("version." + key)));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(File file) {
        if (file == null) {
            return;
        }
        if (!file.exists()) {
            logger.warn(String.format("Properties file [%s] does not exists.", file.getName()));
            return;
        }

        try {
            load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(InputStream inputStream) {
        if (inputStream == null) {
            return;
        }

        Properties props = new Properties();

        try {
            props.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (String key : props.stringPropertyNames()) {
            properties.put(key, props.getProperty(key));
        }

        logProperties(properties);
    }

    private static void logProperties(Map<String, String> properties) {
        if (logger.isTraceEnabled()) {
            for (String key : properties.keySet()) {
                logger.trace(String.format("%s:%s", key, properties.get(key)));
            }
        }
    }

    public Boolean containsKey(String key) {
        return properties.containsKey(key);
    }

    public String get(String key) {
        return get(key, null);
    }

    public String get(String key, String Default) {
        if (properties.containsKey(key))
            return properties.get(key);
        return Default;
    }

    public void put(String key, String value) {
        properties.put(key, value);
    }

    public void clear() {
        properties.clear();
    }
}

package com.heartzones.processor;

import com.google.common.base.Strings;
import com.heartzones.ApplicationProperties;
import com.heartzones.dataManagers.EmailDataManager;
import com.heartzones.models.Email;
import com.sendgrid.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EmailProcessor {

    private static ExecutorService executorService;
    public static Boolean Async = true;

    public static void start(ApplicationProperties properties) {
        executorService = Executors.newFixedThreadPool(Integer.parseInt(properties.get("email.processor.threads", "3")));

        checkforWaitingProcesses(properties);
    }

    private static void checkforWaitingProcesses(ApplicationProperties properties) {
        EmailDataManager emailDataManager = new EmailDataManager(properties);

        List<Email> emails = emailDataManager.list_ReadyToProcess();

        for (Email email : emails) {
            EmailProcessor.process(properties, email);
        }
    }

    public static void process(ApplicationProperties properties, Email email) {
        EmailDataManager emailDataManager = new EmailDataManager(properties);
        emailDataManager.save(1, email);

        if (Async)
            EmailProcessor.executorService.execute(new EmailProcessorWorker(properties, email));
        else {
            EmailProcessorWorker emailProcessorWorker = new EmailProcessorWorker(properties, email);
            emailProcessorWorker.run();
        }
    }
}

class EmailProcessorWorker implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(EmailProcessorWorker.class);

    private ApplicationProperties properties;
    private Email email;

    EmailProcessorWorker(ApplicationProperties properties, Email email) {
        this.properties = properties;
        this.email = email;

    }

    @Override
    public void run() {
        Integer id = email.getId();

        EmailDataManager emialDataManager = new EmailDataManager(properties);

        Map<String, Object> fields = new LinkedHashMap<>();
        fields.put("status", "processing");

        emialDataManager.save(1, id, fields);

        logger.debug(String.format("email [%d] processing", email.getId()));

        // process email here
        SendGrid sendGrid = new SendGrid(properties.get("email.sendgrid.key"));

        if (Strings.isNullOrEmpty(email.getSender()))
            email.setSender(properties.get("email.sender"));

        if (Strings.isNullOrEmpty(email.getSender()))
            throw new NullPointerException("email.sender is not set.");
        if (Strings.isNullOrEmpty(email.getRecipiant()))
            throw new NullPointerException("email.recipiant is not set.");
        if (Strings.isNullOrEmpty(email.getSubject()))
            throw new NullPointerException("email.recipiant is not set.");
        if (Strings.isNullOrEmpty(email.getBody()) && Strings.isNullOrEmpty(email.getTemplate()))
            throw new NullPointerException("email.body is not set.");

        com.sendgrid.Email from = new com.sendgrid.Email(email.getSender());
        com.sendgrid.Email to = new com.sendgrid.Email(email.getRecipiant());
        Content content = null;
        if (!Strings.isNullOrEmpty(email.getBody()))
            content = new Content("text/html", email.getBody());

        Mail mail = new Mail(from, email.getSubject(), to, content);

        if (!Strings.isNullOrEmpty(email.getTemplate())) {
            mail.setTemplateId(email.getTemplate());
        }

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sendGrid.api(request);
        } catch (IOException ex) {
            logger.error("Error Sending email to SendGrid", ex);
        }

        // mark record as sent
        fields.put("status", "sent");
        emialDataManager.save(1, id, fields);
    }
}
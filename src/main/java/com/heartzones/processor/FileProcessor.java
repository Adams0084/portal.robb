package com.heartzones.processor;

import com.heartzones.ApplicationProperties;
import com.heartzones.dataManagers.FileUploadDataManager;
import com.heartzones.models.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileProcessor {
    private static ExecutorService executorService;
    public static Boolean Async = true;

    public static void start(ApplicationProperties properties) {
        executorService = Executors.newFixedThreadPool(Integer.parseInt(properties.get("file.processor.threads", "5")));

        checkforWaitingProcesses(properties);
    }

    private static void checkforWaitingProcesses(ApplicationProperties properties) {
        FileUploadDataManager fileUploadDataManager = new FileUploadDataManager(properties);

        List<FileUpload> fileUploads = fileUploadDataManager.list_ReadyToProcess();

        for (FileUpload fileUpload : fileUploads) {
            FileProcessor.process(properties, fileUpload);
        }
    }

    public static void process(ApplicationProperties properties, FileUpload fileUpload) {
        if (Async) {
            FileProcessor.executorService.execute(new FileProcessorWorker(properties, fileUpload));
        } else {
            FileProcessorWorker fileProcessorWorker = new FileProcessorWorker(properties, fileUpload);
            fileProcessorWorker.run();
        }
    }
}

class FileProcessorWorker implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(FileProcessorWorker.class);

    private ApplicationProperties properties;
    private FileUpload fileUpload;

    FileProcessorWorker(ApplicationProperties properties, FileUpload fileUpload) {
        this.properties = properties;
        this.fileUpload = fileUpload;

        ParticipantsProcessor participantsProcessor = new ParticipantsProcessor(this.properties);
    }

    @Override
    public void run() {
        Integer id = fileUpload.getId();

        FileUploadDataManager fileUploadDataManager = new FileUploadDataManager(properties);

        Map<String, Object> fields = new LinkedHashMap<>();
        fields.put("status", "processing");
        fields.put("start", new Date());

        fileUploadDataManager.save(1, id, fields);

        logger.info(String.format("file [%d] processing : %s", fileUpload.getId(), fileUpload.getFile_name()));

        switch (fileUpload.getFile_type()) {
            case "participants.csv":
                /*
                List<Map<String, Object>> data;
                try {
                    data = CSV.parseJSON(new FileReader(fileUpload.getFile_name()));

                } catch (IOException io) {
                    throw new RuntimeException(String.format("CSV parse error with file [%s]", fileUpload.getFile_name() == null ? "NULL" : fileUpload.getFile_name()), io);
                }
                participantsProcessor.process(fileUpload, data);
                break;
                */
            default:
                fields.clear();
                fields.put("status", "static");
                fields.put("end", new Date());

                fileUploadDataManager.save(1, id, fields);
        }
    }
}

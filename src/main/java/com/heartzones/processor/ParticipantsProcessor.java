package com.heartzones.processor;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountRosterDataManager;
import com.heartzones.dataManagers.FileUploadDataManager;
import com.heartzones.dataManagers.SessionSummaryDataManager;
import com.heartzones.models.AccountRoster;
import com.heartzones.models.FileUpload;
import com.heartzones.models.SessionSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ParticipantsProcessor {
    private final static Logger logger = LoggerFactory.getLogger(ParticipantsProcessor.class);

    private AccountRosterDataManager accountRosterDataManager;
    private SessionSummaryDataManager sessionSummaryDataManager;
    private FileUploadDataManager fileUploadDataManager;

    public ParticipantsProcessor(ApplicationProperties properties) {
        ApplicationProperties properties1 = properties;

        accountRosterDataManager = new AccountRosterDataManager(properties1);
        sessionSummaryDataManager = new SessionSummaryDataManager(properties1);
        fileUploadDataManager = new FileUploadDataManager(properties1);
    }

    public void process(FileUpload fileUpload, List<Map<String,Object>> data) {
        /*TODO: Should probably figure out a way to bulk insert these bad boys */

        for (Map<String,Object> row : data) {
            if (row.containsKey("gender")) {
                if (((String) row.get("gender")).toLowerCase().startsWith("m"))
                    row.put("male", true);
                else
                    row.put("male", false);
                row.remove("gender");
            }
            if (row.containsKey("DOB(M/d/yyyy)")) {
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M/d/yy");
                    row.put("DOB(M/d/yyyy)", simpleDateFormat.parse((String)row.get("DOB(M/d/yyyy)")));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }

            AccountRoster accountRoster = new AccountRoster(fileUpload.getAccount(), fileUpload.getCreate_user());
            DataTools.Objects.to(accountRoster, row);

            SessionSummary sessionSummary = new SessionSummary(fileUpload.getAccount(), fileUpload.getCreate_user());
            DataTools.Objects.to(sessionSummary, accountRoster.getData());

            accountRoster.setData(sessionSummary.getData());

            AccountRoster existingAccountRoster = accountRosterDataManager.get(fileUpload.getCreate_user(), new Parameter("id_local", Operator.EQUALS, accountRoster.getId_local()));
            if (existingAccountRoster != null) {
                Map<String, Object> newRecord = DataTools.Objects.from(accountRoster);
                Map<String, Object> existingRecord = DataTools.Objects.from(existingAccountRoster);

                DataTools.Objects.to(accountRoster, existingRecord);
                DataTools.Objects.to(accountRoster, newRecord);
            }
            else {
                accountRoster.setFile(fileUpload.getId());
            }

            accountRosterDataManager.save(fileUpload.getCreate_user(), accountRoster);

            sessionSummary.setAccount_roster(accountRoster.getId());
            sessionSummary.setFile(fileUpload.getId());
            if (sessionSummary.getStart() == null) sessionSummary.setStart(new Date());
            if (sessionSummary.getEnd() == null) sessionSummary.setEnd(sessionSummary.getStart());

            sessionSummaryDataManager.save(fileUpload.getCreate_user(), sessionSummary);

            Map<String, Object> fileUploadData = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

            fileUploadData.put("end", new Date());
            fileUploadData.put("status", "complete");

            fileUploadDataManager.save(fileUpload.getCreate_user(), fileUpload.getId(), fileUploadData);
        }
    }
}

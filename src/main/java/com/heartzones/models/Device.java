package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.UUID;

public class Device extends DataObject {
    @DataInfo()
    private Integer account;
    @DataInfo(UUIDAsBytes = true)
    private UUID uid;

    public Device() {}
    public Device(Integer account) {
        this.account = account;
    }

    public Integer getAccount() { return account; }

    public UUID getUid() { return uid; }
    public void setUid(UUID uid) { this.uid = uid; }
}

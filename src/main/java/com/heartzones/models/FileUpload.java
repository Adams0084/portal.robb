package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FileUpload extends DataObject {
    @DataInfo()
    private Integer account = null;
    @DataInfo()
    private String file_type = null;
    @DataInfo()
    private String file_name = null;
    @DataInfo(JSON = true)
    private Map<String, Object> meta_data = new HashMap<>();
    @DataInfo()
    private Date start = null;
    @DataInfo()
    private Date end = null;
    @DataInfo()
    private String status = null;
    @DataInfo()
    private String hash = null;

    public FileUpload(){}
    public FileUpload(Integer account){
        this.account = account;
    }
    public FileUpload(Integer account, String hash) {
        this.account = account;
        this.hash = hash;
    }

    public Integer getAccount() {
        return account;
    }

    public String getHash() { return hash; }

    public String getFile_type() {
        return file_type;
    }
    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getFile_name() {
        return file_name;
    }
    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public Map<String, Object> getMeta_data() {
        return meta_data;
    }

    public void setMeta_data(Map<String, Object> meta_data) {
        this.meta_data = meta_data;
    }

    public Date getStart() {
        return start;
    }
    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }
    public void setEnd(Date end) {
        this.end = end;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}

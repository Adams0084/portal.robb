package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class SessionSummary extends DataObject {
    @DataInfo()
    private Integer account = null;
    @DataInfo()
    private Integer account_roster = null;

    @DataInfo()
    private Integer file = null;

    @DataInfo()
    private Date start = null;
    @DataInfo()
    private Date end = null;

    @DataInfo()
    private String fit = null;
    @DataInfo()
    private String hrm = null;
    @DataInfo()
    private String hz_max = null;
    @DataInfo()
    private String hz_shr = null;
    @DataInfo()
    private String hz_t1 = null;
    @DataInfo()
    private String hz_t2 = null;
    @DataInfo()
    private String pwr = null;
    @DataInfo()
    private String sc = null;
    @DataInfo()
    private String sdm = null;
    @DataInfo()
    private String spd = null;
    @DataInfo()
    private String step_max = null;
    @DataInfo()
    private String cad = null;

    @DataInfo(JSON = true)
    private Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    public SessionSummary(){}
    public SessionSummary(Integer account){
        this.account = account;
    }
    public SessionSummary(Integer account, Integer create_user) {
        this.account = account;
        this.create_user = create_user;
    }

    public Integer getAccount() { return account; }

    public Integer getAccount_roster() { return account_roster; }
    public void setAccount_roster(Integer account_roster) { this.account_roster = account_roster; }

    public Integer getFile() { return file; }
    public void setFile(Integer file) { this.file = file; }

    public Date getStart() { return start; }
    public void setStart(Date start) { this.start = start; }

    public Date getEnd() { return end; }
    public void setEnd(Date end) { this.end = end; }

    public String getFit() { return fit; }
    public void setFit(String fit) { this.fit = fit; }

    public String getHrm() { return hrm; }
    public void setHrm(String hrm) { this.hrm = hrm; }

    public String getHz_max() { return hz_max; }
    public void setHz_max(String hz_max) { this.hz_max = hz_max; }

    public String getHz_shr() { return hz_shr; }
    public void setHz_shr(String hz_shr) { this.hz_shr = hz_shr; }

    public String getHz_t1() { return hz_t1; }
    public void setHz_t1(String hz_t1) { this.hz_t1 = hz_t1; }

    public String getHz_t2() { return hz_t2; }
    public void setHz_t2(String hz_t2) { this.hz_t2 = hz_t2; }

    public String getPwr() { return pwr; }
    public void setPwr(String pwr) { this.pwr = pwr; }

    public String getSc() { return sc; }
    public void setSc(String sc) { this.sc = sc; }

    public String getSdm() { return sdm; }
    public void setSdm(String sdm) { this.sdm = sdm; }

    public String getSpd() { return spd; }
    public void setSpd(String spd) { this.spd = spd; }

    public String getStep_max() { return step_max; }
    public void setStep_max(String step_max) { this.step_max = step_max; }

    public String getCad() { return cad; }
    public void setCad(String cad) { this.cad = cad; }

    public Map<String, Object> getData() { return data; }
    public void setData(Map<String, Object> data) { this.data = data; }
}

package com.heartzones.models;

import com.heartzones.data.DataInfo;

public class AccountUser extends DataObject {
    @DataInfo()
    private Integer account;
    @DataInfo()
    private Integer user;
    @DataInfo()
    private String role;

    public AccountUser() {}
    public AccountUser(Integer account) {
        this.account = account;
    }

    public Integer getAccount() { return account; }

    public Integer getUser() { return user; }
    public void setUser(Integer user) { this.user = user; }

    public String getRole() { return role; }
    public void setRole(String role) { this.role = role; }
}

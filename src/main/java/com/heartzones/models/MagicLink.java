package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MagicLink extends DataObject {
    @DataInfo(UUIDAsBytes = true)
    private UUID link = null;
    @DataInfo()
    private String action = null;
    @DataInfo()
    private Date expires = null;
    @DataInfo()
    private Date used = null;
    @DataInfo(JSON = true)
    private Map<String, Object> data = new HashMap<>();

    public MagicLink() {}

    public UUID getLink() {
        return link;
    }
    public void setLink(UUID link) {
        this.link = link;
    }

    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

    public Date getExpires() {
        return expires;
    }
    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public Date getUsed() {
        return used;
    }
    public void setUsed(Date used) {
        this.used = used;
    }

    public Map<String, Object> getData() {
        return data;
    }
    public void setData(Map<String, Object> data) {
        this.data = data;
    }


}

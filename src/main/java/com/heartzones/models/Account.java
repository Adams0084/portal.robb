package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;
import java.util.UUID;

public class Account extends DataObject {
    @DataInfo(UUIDAsBytes = true)
    private UUID uid = null;

    @DataInfo(name = "account_type")
    private String type = null;
    @DataInfo()
    private String name = null;
    @DataInfo()
    private String address_1 = null;
    @DataInfo()
    private String address_2 = null;
    @DataInfo()
    private String city = null;
    @DataInfo()
    private String state = null;
    @DataInfo()
    private String zip = null;
    @DataInfo(Encrypted = true)
    private String phone = null;

    @DataInfo()
    private Integer owner = null;
    @DataInfo(ReadOnly = true, Encrypted = true)
    private String owner_display = null;
    @DataInfo(ReadOnly = true, Encrypted = true)
    private String owner_email = null;
    @DataInfo(ReadOnly = true, Encrypted = true)
    private String owner_first = null;
    @DataInfo(ReadOnly = true, Encrypted = true)
    private String owner_last = null;
    @DataInfo(ReadOnly = true)
    private Date owner_last_logged_in = null;

    @DataInfo(ReadOnly = true)
    private String roles = null;

    @DataInfo(ReadOnly = true)
    private String licenses = null;

    @DataInfo(ReadOnly = true)
    private Date expires = null;

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (AccountTypes.Educational.name().equalsIgnoreCase(type))
            this.type = AccountTypes.Educational.name();
        else if (AccountTypes.Business.name().equalsIgnoreCase(type))
            this.type = AccountTypes.Business.name();
        else if (AccountTypes.Individual.name().equalsIgnoreCase(type))
            this.type = AccountTypes.Individual.name();
        else
            throw new RuntimeException("Invalid Account Type");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        roles = roles;
    }

    public String getLicenses() {
        return licenses;
    }

    public void setLicenses(String licenses) {
        this.licenses = licenses;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public Integer getOwner() {
        return owner;
    }

    public String getOwner_display() {
        return owner_display;
    }

    public String getOwner_email() {
        return owner_email;
    }

    public String getOwner_first() {
        return owner_first;
    }

    public String getOwner_last() {
        return owner_last;
    }

    public Date getOwner_last_logged_in() {
        return owner_last_logged_in;
    }
}

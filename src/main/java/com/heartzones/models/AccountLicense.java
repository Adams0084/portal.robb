package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;

public class AccountLicense extends DataObject {
    @DataInfo()
    private Integer account;
    @DataInfo()
    private String name;
    @DataInfo(DateOnly = true)
    private Date expires;
    @DataInfo()
    private String features;

    public AccountLicense() {}
    public AccountLicense(Integer account) {
        this.account = account;
    }

    public Integer getAccount() {
        return account;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
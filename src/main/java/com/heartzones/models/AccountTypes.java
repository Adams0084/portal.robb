package com.heartzones.models;

public enum AccountTypes {
    Educational, Business, Individual
}
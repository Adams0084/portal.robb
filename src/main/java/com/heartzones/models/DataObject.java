package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;

public abstract class DataObject {
    @DataInfo()
    protected Integer id = null;

    @DataInfo()
    protected Date created = null;
    @DataInfo()
    protected Integer create_user = null;
    @DataInfo()
    protected Date updated = null;
    @DataInfo()
    protected Integer update_user = null;
    @DataInfo()
    protected Boolean deleted;

    public Integer getId() {
        return id;
    }

    public Date getCreated() { return created; }
    public Integer getCreate_user() { return create_user; }

    public Date getUpdated() {
        return updated;
    }
    public Integer getUpdate_user() { return update_user; }

    public Boolean getDeleted() { return deleted; }
    public void setDeleted(Boolean deleted) { this.deleted = deleted; }
}

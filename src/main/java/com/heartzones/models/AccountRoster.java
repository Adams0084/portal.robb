package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class AccountRoster extends DataObject {
    @DataInfo()
    private Integer account = null;
    @DataInfo()
    private Integer file = null;
    @DataInfo()
    private Integer user = null;
    @DataInfo(Aliases = "DOB(M/d/yyyy)", DateOnly = true)
    private Date dob = null;
    @DataInfo(Encrypted = true)
    private String email = null;
    @DataInfo(Encrypted = true, Aliases = "First Name")
    private String first = null;
    @DataInfo(Encrypted = true, Aliases = "Last Name")
    private String last = null;
    @DataInfo(Encrypted = true, Aliases = "Nick Name")
    private String nick = null;
    @DataInfo()
    private Boolean male = null;
    @DataInfo(Aliases = "Height(inches)")
    private String height = null;
    @DataInfo(Aliases = "StudentID")
    private String id_local = null;
    @DataInfo(Aliases = "Weight(lbs)")
    private String weight = null;
    @DataInfo(JSON = true)
    private Map<String, Object> data = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    public AccountRoster(){}
    public AccountRoster(Integer account){
        this.account = account;
    }
    public AccountRoster(Integer account, Integer create_user) {
        this.account = account;
        this.create_user = create_user;
    }

    public Integer getAccount() { return account; }

    public Integer getFile() { return file; }
    public void setFile(Integer file) { this.file = file; }

    public Integer getUser() { return user; }
    public void setUser(Integer user) { this.user = user; }

    public Date getDOB() { return dob; }
    public void setDOB(Date dob) { this.dob = dob; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getFirst() { return first; }
    public void setFirst(String first) { this.first = first; }

    public String getLast() { return last; }
    public void setLast(String last) { this.last = last; }

    public String getNick() { return nick; }
    public void setNick(String nick) { this.nick = nick; }

    public Boolean getMale() { return male; }
    public void setMale(Boolean male) { this.male = male; }

    public String getHeight() { return height; }
    public void setHeight(String height) { this.height = height; }

    public String getId_local() { return id_local; }
    public void setId_local(String id_local) { this.id_local = id_local; }

    public String getWeight() { return weight; }
    public void setWeight(String weight) { this.weight = weight; }

    public Map<String, Object> getData() { return data; }
    public void setData(Map<String, Object> data) { this.data = data; }
}

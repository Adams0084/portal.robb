package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;

public class Session extends DataObject {
    @DataInfo()
    private String name = null;
    @DataInfo()
    private String location = null;
    @DataInfo()
    private Integer account = null;
    @DataInfo()
    private Integer teacher = null;
    @DataInfo()
    private Date start = null;
    @DataInfo()
    private Date end = null;

    public Session() {}
    public Session(Integer account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAccount() {
        return account;
    }

    public Integer getTeacher() {
        return teacher;
    }

    public void setTeacher(Integer teacher) {
        this.teacher = teacher;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}

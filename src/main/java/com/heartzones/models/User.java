package com.heartzones.models;

import com.google.common.base.Strings;
import com.heartzones.data.DataInfo;

import java.util.*;

public class User extends DataObject {
    @DataInfo()
    private Date verified = null;

    @DataInfo()
    private String avatar = null;

    @DataInfo(Encrypted = true)
    private String first = null;
    @DataInfo(Encrypted = true)
    private String last = null;
    @DataInfo(Encrypted = true)
    private String nick = null;
    @DataInfo()
    private String middle = null;
    @DataInfo(Encrypted = true, StoreLowerCase = true)
    private String username = null;
    @DataInfo(Encrypted = true)
    private String display = null;
    @DataInfo()
    private Boolean admin = false;
    @DataInfo()
    private Boolean male = null;

    @DataInfo(JSON = true)
    private Map<String, Object> preferences = new HashMap<>();

    private List<String> roles = new ArrayList<>();
    @DataInfo()
    private String external_id = null;
    @DataInfo()
    private Date last_logged_in = null;

    @DataInfo()
    private Boolean optional_info = false;

    @DataInfo()
    private Date terms_agreed = null;

    @DataInfo(Encrypted = true)
    private String phone = null;
    @DataInfo(Encrypted = true)
    private String address_1 = null;
    @DataInfo(Encrypted = true)
    private String address_2 = null;
    @DataInfo()
    private String city = null;
    @DataInfo()
    private String state = null;
    @DataInfo()
    private String zip = null;

    @DataInfo()
    private String dob = null;

    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFirst() {
        return first;
    }
    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }
    public void setLast(String last) {
        this.last = last;
    }

    public String getMiddle() {
        return middle;
    }
    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplay() {
        if (Strings.isNullOrEmpty(display)) {
            return String.format("%s, %s", last, first);
        }

        return display;
    }
    public void setDisplay(String display) {
        this.display = display;
    }

    public Map<String, Object> getPreferences() { return preferences; }

    public void setPreferences(Map<String, Object> preferences) { this.preferences = preferences; }

    public Boolean getAdmin() { return admin; }

    public void setAdmin(Boolean admin) { this.admin = admin; }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public Date getLast_logged_in() {
        return last_logged_in;
    }
    public void setLast_logged_in(Date last_logged_in) { this.last_logged_in = last_logged_in; }

    public Boolean getOptional_info() { return optional_info; }
    public void setOptional_info(Boolean optional_info) { this.optional_info = optional_info; }

    public Date getTerms_agreed() { return terms_agreed; }
    public void setTerms_agreed(Date terms_agreed) { this.terms_agreed = terms_agreed; }

    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }

    public String getAddress_1() { return address_1; }
    public void setAddress_1(String address_1) { this.address_1 = address_1; }

    public String getAddress_2() { return address_2; }

    public void setAddress_2(String address_2) { this.address_2 = address_2; }

    public String getCity() { return city; }
    public void setCity(String city) { this.city = city; }

    public String getState() { return state; }
    public void setState(String state) { this.state = state; }

    public String getZip() { return zip; }
    public void setZip(String zip) { this.zip = zip; }

    public Date getVerified() { return verified; }
    public void setVerified(Date verified) { this.verified = verified; }

    public String getDob() { return dob; }
    public void setDob(String dob) { this.dob = dob; }

    public String getNick() { return nick; }
    public void setNick(String nick) { this.nick = nick; }

    public Boolean getMale() { return male; }
    public void setMale(Boolean male) { this.male = male; }
}

package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Email extends DataObject {
    @DataInfo()
    private Integer account = null;
    @DataInfo()
    private Date sent = null;
    @DataInfo(Encrypted = true)
    private String recipiant = null;
    @DataInfo(Encrypted = true)
    private String sender = null;
    @DataInfo()
    private String subject = null;
    @DataInfo()
    private String body = null;
    @DataInfo()
    private String status = "ready";
    @DataInfo()
    private String template = null;
    @DataInfo(JSON = true)
    private Map<String, Object> data = new HashMap<>();

    public Integer getAccount() { return account; }
    public void setAccount(Integer account) { this.account = account; }

    public Date getSent() { return sent; }
    public void setSent(Date sent) { this.sent = sent; }

    public String getRecipiant() { return recipiant; }
    public void setRecipiant(String recipiant) { this.recipiant = recipiant; }

    public String getSender() { return sender; }
    public void setSender(String sender) {this.sender = sender; }

    public String getSubject() { return subject; }
    public void setSubject(String subject) { this.subject = subject; }

    public String getBody() { return body; }
    public void setBody(String body) { this.body = body; }

    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    public String getTemplate() { return template; }
    public void setTemplate(String template) { this.template = template; }

    public Map<String, Object> getData() { return data; }
    public void setData(Map<String, Object> data) { this.data = data; }
}

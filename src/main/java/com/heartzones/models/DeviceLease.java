package com.heartzones.models;

import com.heartzones.data.DataInfo;

import java.util.Date;

public class DeviceLease extends DataObject {
    @DataInfo()
    private Integer device;
    @DataInfo()
    private Integer roster;

    @DataInfo()
    private Date start;
    @DataInfo()
    private Date end;

    public Integer getDevice() { return device; }
    public void setDevice(Integer device) { this.device = device; }

    public Integer getRoster() { return roster; }
    public void setRoster(Integer roster) { this.roster = roster; }

    public Date getStart() { return start; }
    public void setStart(Date start) { this.start = start; }

    public Date getEnd() { return end; }
    public void setEnd(Date end) { this.end = end; }
}

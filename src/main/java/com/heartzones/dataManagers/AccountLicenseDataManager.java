package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.models.AccountLicense;
import org.slf4j.LoggerFactory;

public class AccountLicenseDataManager extends DataManager<AccountLicense> {
    public AccountLicenseDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "account_license";
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
    }
}

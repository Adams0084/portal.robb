package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.models.Session;
import org.slf4j.LoggerFactory;

public class SessionDataManager extends DataManager<Session>{
    public SessionDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "session";
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
    }
}

package com.heartzones.dataManagers;


import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.models.MagicLink;
import org.slf4j.LoggerFactory;

public class MagicLinkDataManager extends DataManager<MagicLink> {
    public MagicLinkDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "magic_link";
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
        this.authorization_account = false;
        this.authorization_user_param = false;
    }
}

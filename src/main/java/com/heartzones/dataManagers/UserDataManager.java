package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.data.DataInfoImpl;
import com.heartzones.models.User;
import org.slf4j.LoggerFactory;

public class UserDataManager extends DataManager<User> {
    public UserDataManager(ApplicationProperties properties) {
        super();

        this.dataInfos.add(new DataInfoImpl("password").setPassword(true));

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
        this.authorization_account = false;
        this.authorization_user_param = true;
        this.tableName = "user";

        StringBuilder sb = new StringBuilder();
        sb.append("(SELECT u.* ");
        sb.append("FROM user u ");
        sb.append("JOIN (SELECT id, admin from user WHERE id = ?) AS ua ON ua.id = u.id or ua.admin = 1)");

        /*
        * Trick it to build the select with a different "table" than the Insert and Update
        */
        this.statement_select = statementSelect().replace("{{TABLE}}", sb.toString());
    }
}

package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.models.AccountUser;
import org.slf4j.LoggerFactory;

public class AccountUserDataManager extends DataManager<AccountUser> {
    public AccountUserDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "account_user";
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
    }
}

package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.data.DataTools;
import com.heartzones.models.FileUpload;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FileUploadDataManager extends DataManager<FileUpload> {
    public FileUploadDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "files";
        this.logger = LoggerFactory.getLogger(FileUploadDataManager.class);
    }

    public List<FileUpload> list_ReadyToProcess() {
        String query = "SELECT * FROM files WHERE status = 'loaded' OR (status = 'processing' AND end IS NULL AND start < ?)";

        Map<String, Object>  parameters_query = new LinkedHashMap<>();
        parameters_query.put("updated", DataTools.Dates.addMinutes(new Date(), -1 * Integer.parseInt(properties.get("file.processor.retry.minutes", "5"))));

        List<Map<String, Object>> rows = connectionManager.get().executeQuery(query.toString(), parameters_query);
        List<FileUpload> rtn = objectBuilder(rows);
        process_post(rtn);

        return rtn;
    }
}

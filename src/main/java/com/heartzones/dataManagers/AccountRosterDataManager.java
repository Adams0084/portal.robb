package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.models.AccountRoster;
import org.slf4j.LoggerFactory;

public class AccountRosterDataManager extends DataManager<AccountRoster> {
    public AccountRosterDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "account_roster";
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
    }
}

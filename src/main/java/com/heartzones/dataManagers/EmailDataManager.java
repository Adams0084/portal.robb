package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.data.DataTools;
import com.heartzones.models.Email;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class EmailDataManager extends DataManager<Email> {
    public EmailDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "email";
        this.logger = LoggerFactory.getLogger(FileUploadDataManager.class);
        this.authorization_user_param = false;
        this.authorization_account = false;
    }

    public List<Email> list_ReadyToProcess() {
        String query = "SELECT * FROM email WHERE status = 'ready' OR (status = 'processing' AND updated < ?)";

        Map<String, Object>  parameters_query = new LinkedHashMap<>();
        parameters_query.put("updated", DataTools.Dates.addMinutes(new Date(), -1 * Integer.parseInt(properties.get("email.processor.retry.minutes", "5"))));

        List<Map<String, Object>> rows = connectionManager.get().executeQuery(query.toString(), parameters_query);

        List<Email> rtn = objectBuilder(rows);
        process_post(rtn);

        return rtn;
    }
}

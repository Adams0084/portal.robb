package com.heartzones.dataManagers;

import com.google.common.base.Strings;
import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.data.DataTools;
import com.heartzones.models.Account;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AccountDataManager extends DataManager<Account> {
    public AccountDataManager(ApplicationProperties properties) {
        super();

        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
        this.authorization_account = false;
        this.authorization_user_param = true;
        this.tableName = "account";

        StringBuilder sb = new StringBuilder();

        sb.append("(SELECT a.*, t.roles, t.licenses, t.expires, t.owner, t.owner_display, owner_email, owner_first, owner_last, owner_last_logged_in ");
        sb.append("FROM account as a ");
        sb.append("JOIN ");
        sb.append("(SELECT a.id, ");
        sb.append("coalesce(GROUP_CONCAT(DISTINCT au.role), CASE u.admin WHEN 1 THEN 'system' END) AS roles, ");
        sb.append("coalesce(GROUP_CONCAT(DISTINCT al.name)) AS licenses, MAX(al.expires) AS expires, ");
        sb.append("MAX(own.id) as owner, ");
        sb.append("MAX(own.display) as owner_display, ");
        sb.append("MAX(own.username) as owner_email, ");
        sb.append("MAX(own.first) as owner_first, ");
        sb.append("MAX(own.last) as owner_last, ");
        sb.append("MAX(own.last_logged_in) as owner_last_logged_in ");
        sb.append("FROM account AS a ");
        sb.append("JOIN user AS u ON u.id = ? AND u.deleted = 0 ");
        sb.append("LEFT JOIN account_user AS au ON au.account = a.id AND au.user = u.id AND au.deleted = 0 ");
        sb.append("LEFT JOIN account_user AS ao ON ao.account = a.id AND ao.role = 'owner' AND ao.deleted = 0 ");
        sb.append("LEFT JOIN user AS own ON own.id = ao.user AND own.deleted = 0 ");
        sb.append("LEFT JOIN account_license AS al ON al.account = a.id AND al.deleted = 0 AND al.expires > now() ");
        sb.append("GROUP BY a.id) as t ON t.id = a.id AND t.roles IS NOT NULL) ");

        /*
        * Trick it to build the select with a different "table" than the Insert and Update
        */
        this.statement_select = statementSelect().replace("{{TABLE}}", sb.toString());
    }

    public List<Account> process_post(List<Account> data) {
        for (Account a: data) {
            if (Strings.isNullOrEmpty(a.getOwner_display()))
                DataTools.Objects.setValue(a, "owner_display", String.format("%s, %s", a.getOwner_last(), a.getOwner_first()));
        }
        return data;
    }
}

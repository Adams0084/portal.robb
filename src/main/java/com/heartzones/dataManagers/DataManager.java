package com.heartzones.dataManagers;

import com.google.common.base.Strings;
import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.data.DataInfo;
import com.heartzones.data.DataTools;
import com.heartzones.data.Parameter;
import com.heartzones.models.DataObject;
import org.slf4j.Logger;

import java.lang.reflect.ParameterizedType;
import java.util.*;

import static com.heartzones.data.DataTools.Persistence.dataInfoSet;

public abstract class DataManager<T extends DataObject> {
    Logger logger;
    ConnectionManager connectionManager;
    ApplicationProperties properties;
    String tableName;
    String tableAlias = "t";
    /**
     * {{TABLEALIAS}} for alias replacement
     * {{TABLE}} for tablename replacement
     * {{WHERE}} for where clause insertion
     * variable @Admin to check if the current user is an admin
     * table user_accounts as ua for current user accounts and roles (comma separated varchar)
     */

    String statement_select;
    Boolean authorization_account = true;
    Boolean authorization_user_param = true;

    List<DataInfo> dataInfos = new ArrayList<>();

    DataManager() {
        dataInfos.addAll(dataInfoSet(getInstance()));
    }

    String statementDelete() {
        StringBuilder sb = new StringBuilder();

        sb.append("DELETE FROM {{TABLE}} ");
        sb.append("{{WHERE}};");

        return sb.toString();
    }

    String statementSelect() {
        if (Strings.isNullOrEmpty(statement_select)) {
            StringBuilder sb = new StringBuilder();

            if (authorization_account) {
                sb.append("SELECT {{TABLEALIAS}}.* ");
                sb.append("FROM {{TABLE}} AS {{TABLEALIAS}} ");
                sb.append("JOIN (SELECT a.id, ");
                sb.append("coalesce(GROUP_CONCAT(au.role), CASE u.admin WHEN 1 THEN 'system' END) AS roles ");
                sb.append("FROM account AS a ");
                sb.append("JOIN user AS u ON u.id = ? AND u.deleted = 0 ");
                sb.append("LEFT JOIN account_user AS au ON au.account = a.id AND au.user = u.id AND au.deleted = 0 ");
                sb.append("JOIN account_license AS al ON al.account = a.id AND al.expires > now() AND al.deleted = 0 ");
                sb.append("GROUP BY a.id) AS ua ON ua.id = {{TABLEALIAS}}.account AND ua.roles IS NOT NULL ");
                sb.append("{{WHERE}};");
            } else {
                sb.append("SELECT {{TABLEALIAS}}.* ");
                sb.append("FROM {{TABLE}} AS {{TABLEALIAS}} ");
                sb.append("{{WHERE}};");
            }
            statement_select = sb.toString();

            logger.trace(String.format("SELECT STATEMENT : ", statement_select));
        }

        return statement_select;
    }

    List<T> process_post(List<T> data) {
        return data;
    }

    Class<T> classT;
    T getInstance() {
        if (classT == null) {
            ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
            classT = (Class<T>) superClass.getActualTypeArguments()[0];
        }

        try {

            return classT.newInstance();

        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    List<T> objectBuilder(List<Map<String, Object>> rows) {
        List<T> rtn = new ArrayList<>();
        for (Map<String, Object> row : rows) {
            T obj = getInstance();
            Map<String, Object> raw = DataTools.Persistence.from(dataInfos, row, properties);
            DataTools.Objects.to(obj, raw);
            rtn.add(obj);
        }
        return rtn;
    }
    public T get(Integer user, Parameter parameter) {
        Set<Parameter> parameters = new LinkedHashSet<>();
        parameters.add(parameter);

        return get(user, parameters);
    }
    public T get(Integer user, Set<Parameter> params) {
        List<T> rtn = list(user, params);

        if (rtn != null && rtn.size() > 0)
            return rtn.get(0);
        return null;
    }
    public List<T> list(Integer user, Parameter parameter) {
        Set<Parameter> parameters = new LinkedHashSet<>();
        parameters.add(parameter);

        return list(user, parameters);
    }
    public List<T> list(Integer user, Set<Parameter> params) {
        List<T> rtn;

        //prevent changing passed in Set
        Set<Parameter> parameters = new LinkedHashSet<>(params);
        DataTools.Persistence.mapParameters(dataInfos, parameters, properties);

        String statement = statementSelect();
        String where = DataTools.DB.whereBuilder(tableAlias, parameters);

        statement = statement.replace("{{TABLE}}", tableName);
        statement = statement.replace("{{WHERE}}", where);
        statement = statement.replace("{{TABLEALIAS}}", tableAlias);

        parameters.removeIf(p -> p.getValue() == null);

        Map<String, Object>  parameters_query = new LinkedHashMap<>();

        if (authorization_account || authorization_user_param)
            parameters_query.put("user_accounts", user);

        for (Parameter p: parameters)
            parameters_query.put(p.getName(), p.getValue());

        List<Map<String, Object>> rows = connectionManager.get().executeQuery(statement.toString(), parameters_query);

        rtn = objectBuilder(rows);

        process_post(rtn);

        return rtn;
    }
    public void save(Integer user, T obj) {
        Map<String, Object> values = DataTools.Objects.from(obj);

        Integer id = null;
        if (values.containsKey("id"))
            id = (Integer) values.get("id");

        Map<String, Object> rtn = save(user, id, values);

        if (DataTools.Objects.getValue(obj, "id") == null && rtn.containsKey("id"))
            DataTools.Objects.setValue(obj, "id", rtn.get("id"));
        if (DataTools.Objects.getValue(obj, "created") == null && rtn.containsKey("created"))
            DataTools.Objects.setValue(obj, "created", rtn.get("created"));
        if (DataTools.Objects.getValue(obj, "updated") == null && rtn.containsKey("updated"))
            DataTools.Objects.setValue(obj, "updated", rtn.get("updated"));
        if (DataTools.Objects.getValue(obj, "create_user") == null && rtn.containsKey("create_user"))
            DataTools.Objects.setValue(obj, "create_user", rtn.get("create_user"));
        if (DataTools.Objects.getValue(obj, "update_user") == null && rtn.containsKey("update_user"))
            DataTools.Objects.setValue(obj, "update_user", rtn.get("update_user"));
    }
    public Map<String, Object> save(Integer user, Map<String, Object> data) {
        return save(user, null, data);
    }
    public Map<String, Object> save(Integer user, Integer id, Map<String, Object> data) {
        Map<String, Object> values = DataTools.Persistence.to(dataInfos, data, properties);
        if (id == null)
            return insert(user, values);
        else
            return update(user, id, values);
    }
    public void delete(Integer user, Parameter parameter) {
        Set<Parameter> parameters = new LinkedHashSet<>();
        parameters.add(parameter);

        delete(user, parameters);
    }
    public void delete(Integer user, Set<Parameter> params) {
        Set<Parameter> parameters = new LinkedHashSet<>(params);
        DataTools.Persistence.mapParameters(dataInfos, parameters, properties);

        String statement = statementDelete();
        String where = DataTools.DB.whereBuilder("", parameters);

        statement = statement.replace("{{TABLE}}", tableName);
        statement = statement.replace("{{WHERE}}", where);
        statement = statement.replace("{{TABLEALIAS}}", tableAlias);

        parameters.removeIf(p -> p.getValue() == null);

        Map<String, Object>  parameters_query = new LinkedHashMap<>();

        for (Parameter p: parameters)
            parameters_query.put(p.getName(), p.getValue());

        connectionManager.get().execute(statement, parameters_query);
    }

    private Map<String, Object> update(Integer user, Integer id, Map<String, Object> values) {
        StringBuilder statement = new StringBuilder();
        Map<String, Object> _values = new LinkedHashMap(values);

        _values.remove("id");

        _values.put("updated", new Date());
        _values.put("update_user", user);

        statement.append("UPDATE ");
        statement.append(this.tableName);
        statement.append(" SET ");

        Boolean first = true;
        for (String k : _values.keySet()) {
            if (!first) statement.append(",");
            first = false;
            statement.append(k);
            statement.append("=?");
        }

        statement.append(" WHERE id = ?;");

        _values.put("id", id);

        connectionManager.get().execute(statement.toString(), _values);

        return _values;
    }

    private Map<String, Object> insert(Integer user, Map<String, Object> values) {
        StringBuilder statement = new StringBuilder();
        Map<String,Object> _values = new LinkedHashMap(values);

        _values.put("created", new Date());
        _values.put("create_user", user);

        statement.append("INSERT INTO ");
        statement.append(this.tableName);
        statement.append(" (");

        Boolean first = true;
        for (String k : _values.keySet()) {
            if (!first) statement.append(",");
            first = false;
            statement.append(k);
        }

        statement.append(") VALUES (");

        first = true;
        for (String k : _values.keySet()) {
            if (!first) statement.append(",");
            first = false;
            statement.append("?");
        }

        statement.append(");");

        Integer id = connectionManager.get().execute(statement.toString(), _values);

        _values.put("id", id);

        return _values;
    }
}

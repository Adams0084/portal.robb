package com.heartzones.dataManagers;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import com.heartzones.models.SessionSummary;
import org.slf4j.LoggerFactory;

public class SessionSummaryDataManager extends DataManager<SessionSummary> {
    public SessionSummaryDataManager(ApplicationProperties properties) {
        super();
        this.properties = properties;
        this.connectionManager = new ConnectionManager(properties);
        this.tableName = "session_summary";
        this.logger = LoggerFactory.getLogger(SessionDataManager.class);
    }
}
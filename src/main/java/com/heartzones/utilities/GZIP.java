package com.heartzones.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZIP {
    private final static Logger logger = LoggerFactory.getLogger(GZIP.class);

    private static void compress(InputStream inStream, OutputStream outStream) {
        try {
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(outStream);
            byte[] buffer = new byte[1024];
            int len;
            while((len=inStream.read(buffer)) != -1){
                gzipOutputStream.write(buffer, 0, len);
            }
            //close resources
            gzipOutputStream.close();
            outStream.close();
            inStream.close();
        } catch (IOException e) {
            logger.error("IOException compressing File", e);
        }
    }
    private static void decompress(InputStream inStream, OutputStream outStream) {
        try {
            GZIPInputStream gzipInputStream = new GZIPInputStream(inStream);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gzipInputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            //close resources
            outStream.close();
            gzipInputStream.close();
        } catch (IOException e) {
            logger.error("IOException decompressing File", e);
        }
    }

    private static void compressFile(String file, String outfile) {
        try {
            InputStream inputStream = new FileInputStream(file);
            OutputStream outputStream = new FileOutputStream(outfile);

            GZIP.compress(inputStream, outputStream);
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException compressing File", e);
        }
    }

    private static void decompressFile(String gzipFile, String newFile) {
        try {
            FileInputStream fis = new FileInputStream(gzipFile);
            GZIPInputStream gis = new GZIPInputStream(fis);
            FileOutputStream fos = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            //close resources
            fos.close();
            gis.close();
        } catch (IOException e) {
            logger.error("IOException decompressing File", e);
        }
    }
}
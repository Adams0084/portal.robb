package com.heartzones.utilities;

import au.com.bytecode.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.*;

/***
 * wrapper around CSV parser in case we need to adjust the technology
 */
public class CSV {
    private final static Logger logger = LoggerFactory.getLogger(CSV.class);

    public static List<List<String>> parse(Reader reader) throws IOException {
        List<List<String>> rtn = new ArrayList<>();
        CSVReader csvReader = new CSVReader(reader);

        String[] nextLine;
        while ((nextLine = csvReader.readNext()) != null) {
            rtn.add(Arrays.asList(nextLine));
        }

        return rtn;
    }

    /***
     * parse a csv reader that contains headers into a map data structure
     * @param reader
     * @return
     * @throws IOException
     */
    public static List<Map<String, Object>> parseJSON(Reader reader) throws IOException {
        return parseJSON(reader, null);
    }
    /***
     * parse a csv reader into a map data structure using provided headers
     * @param reader
     * @param headers
     * @return
     * @throws IOException
     */
    public static List<Map<String, Object>> parseJSON(Reader reader, List<String> headers) throws IOException {
        List<Map<String, Object>> rtn = new ArrayList<>();

        List<List<String>> data = parse(reader);

        for (List<String> row: data) {
            if (headers == null || headers.size() == 0) {
                headers = row;
            }
            else {
                Map<String, Object> rowMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
                for (String header: headers) {
                    rowMap.put(header, null);
                }

                for (Integer i = 0; i < row.size(); i++) {
                    if (headers.size() > i) {
                        rowMap.put(headers.get(i), row.get(i));
                    } else {
                        rowMap.put(String.format("column_%d", i), row.get(i));
                    }
                }

                rtn.add(rowMap);
            }
        }

        return rtn;
    }
}

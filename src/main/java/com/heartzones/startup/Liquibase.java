package com.heartzones.startup;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.ConnectionManager;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public class Liquibase {
    private final static Logger logger = LoggerFactory.getLogger(Liquibase.class);
    private static boolean executed = false;

    public void start(ApplicationProperties properties) {
        if (executed) return;
        if (!Boolean.parseBoolean(properties.get("liquibase.enabled", "true"))) {
            logger.warn("Liquibase is turned off.");
            return;
        }

        try {
            Connection connection = new ConnectionManager(properties).get().connection();

            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));

            liquibase.Liquibase liquibase = new liquibase.Liquibase("liquibase/_changelog-master.xml", new ClassLoaderResourceAccessor(), database);

            if (Boolean.parseBoolean(properties.get("database.drop", "false")))
                liquibase.dropAll();

            liquibase.update(new Contexts(), new LabelExpression());

            connection.close();
            executed = true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
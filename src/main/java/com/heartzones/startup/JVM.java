package com.heartzones.startup;

import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimeZone;

public class JVM {
    private final static Logger logger = LoggerFactory.getLogger(JVM.class);

    public void start(ApplicationProperties properties) {
        //set Timezone to UTC
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}

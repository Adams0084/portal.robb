package com.heartzones.startup;

import com.heartzones.ApplicationProperties;
import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountDataManager;
import com.heartzones.dataManagers.AccountLicenseDataManager;
import com.heartzones.dataManagers.AccountUserDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class InitialData {
    private final static Logger logger = LoggerFactory.getLogger(InitialData.class);

    private ApplicationProperties properties;
    private UserDataManager userDataManager;
    private AccountDataManager accountDataManager;
    private AccountUserDataManager accountUserDataManager;
    private AccountLicenseDataManager accountLicenseDataManager;

    //test account should always use same uid so it can be found
    private UUID testAccountUid = UUID.fromString("c290f69a-208e-43c2-8460-7b90884cbf4d");

    public void start(ApplicationProperties properties) {
        userDataManager = new UserDataManager(properties);
        accountDataManager = new AccountDataManager(properties);
        accountUserDataManager = new AccountUserDataManager(properties);
        accountLicenseDataManager = new AccountLicenseDataManager(properties);

        this.properties = properties;

        userSystem();
        userJoe();
        userShawn();
        userStaff();
        userCorene();
        userRick();
        userSally();

        Boolean createTestAccounts = Boolean.parseBoolean(properties.get("test.users", "false"));
        if (createTestAccounts) {
            testAcccount();

            userOwner();
            userAdmin();
            userTeacher();
            userStudent();
        }

        userName_caseInsensitive();
    }

    private void userSystem() {
        String userName = "system@bca.net";
        String password = "S73cW;yq+f;F";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", true);
            data.put("first", "System");
            data.put("last", "User");
            data.put("verified", new Date());
            data.put("male", false);

            userDataManager.save(1, data);
        }
    }

    private void userSally() {
        String userName = "sally.edwards@heartzones.com";
        String password = "MyHeartZones101!";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", true);
            data.put("first", "Sally");
            data.put("last", "Edwards");
            data.put("verified", new Date());
            data.put("male", false);

            userDataManager.save(1, data);
        }
    }

    private void userJoe() {
        String userName = "joe@heartzones.com";
        String password = "V0lleyball!";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", true);
            data.put("first", "Joe");
            data.put("last", "Gooden");
            data.put("verified", new Date());
            data.put("male", true);

            userDataManager.save(1, data);
        }
    }

    private void userShawn() {
        String userName = "shawn.thompson@heartzones.com";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user != null) {
            userDataManager.delete(1,  new Parameter("username", Operator.EQUALS, userName));
        }
    }

    private void userStaff() {
        String userName = "staff@heartzones.com";
        String password = "Heartzones1!";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", true);
            data.put("first", "Staff");
            data.put("last", "Heartzones");
            data.put("verified", new Date());
            data.put("male", true);

            userDataManager.save(1, data);
        }
    }

    private void userCorene() {
        String userName = "corene.marshalek@heartzones.com";
        String password = "7ec00acc";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", true);
            data.put("first", "Corene");
            data.put("last", "Marshalek");
            data.put("verified", new Date());
            data.put("male", false);

            userDataManager.save(1, data);
        }
    }

    private void userRick() {
        String userName = "rickg@npe-inc.com";
        String password = "iosapp!";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", true);
            data.put("first", "Rick");
            data.put("last", "Gibbs");
            data.put("verified", new Date());
            data.put("male", true);

            userDataManager.save(1, data);
        }
    }

    private void testAcccount() {
        String accountName = "Heartzones: Test Account";
        Account account = accountDataManager.get(1, new Parameter("uid", Operator.EQUALS, testAccountUid));

        if (account != null && !account.getName().equalsIgnoreCase(accountName))
            throw new RuntimeException(String.format("Account [1] must be the [%s].  Persistence is wrong.", accountName));

        if (account == null) {
            account = new Account();

            account.setName(accountName);
            account.setType(AccountTypes.Educational.name());
            account.setUid(testAccountUid);

            accountDataManager.save(1, account);

            AccountLicense accountLicense = new AccountLicense(account.getId());
            accountLicense.setExpires(DataTools.Dates.addDays(new Date(), Integer.parseInt(properties.get("account.license.temp.days"))));
            accountLicense.setName(properties.get("account.license.temp.name"));
            accountLicense.setFeatures(properties.get("account.license.temp.features"));

            accountLicenseDataManager.save(1, accountLicense);
        }
    }

    private void userStudent() {
        String userName = "user@bca.net";
        String password = "password";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));
        Account account = accountDataManager.get(1, new Parameter("uid", Operator.EQUALS, testAccountUid));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", false);
            data.put("first", "Student");
            data.put("last", "Heartzones");
            data.put("verified", new Date());
            data.put("male", true);

            Map<String, Object> rtn = userDataManager.save(1, data);

            AccountUser accountUser = new AccountUser(account.getId());
            accountUser.setUser((Integer) rtn.get("id"));
            accountUser.setRole("student");

            accountUserDataManager.save(1, accountUser);
        }
    }

    private void userTeacher() {
        String userName = "teacher@bca.net";
        String password = "password";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));
        Account account = accountDataManager.get(1, new Parameter("uid", Operator.EQUALS, testAccountUid));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", false);
            data.put("first", "Teacher");
            data.put("last", "Heartzones");
            data.put("verified", new Date());
            data.put("male", true);

            Map<String, Object> rtn = userDataManager.save(1, data);

            AccountUser accountUser = new AccountUser(account.getId());
            accountUser.setUser((Integer) rtn.get("id"));
            accountUser.setRole("teacher");

            accountUserDataManager.save(1, accountUser);
        }
    }

    private void userOwner() {
        String userName = "owner@bca.net";
        String password = "password";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));
        Account account = accountDataManager.get(1, new Parameter("uid", Operator.EQUALS, testAccountUid));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", false);
            data.put("first", "Owner");
            data.put("last", "Heartzones");
            data.put("verified", new Date());
            data.put("male", false);

            Map<String, Object> rtn = userDataManager.save(1, data);

            AccountUser accountUser = new AccountUser(account.getId());
            accountUser.setUser((Integer) rtn.get("id"));
            accountUser.setRole("owner");

            accountUserDataManager.save(1, accountUser);
        }
    }

    private void userAdmin() {
        String userName = "admin@bca.net";
        String password = "password";

        User user = userDataManager.get(1, new Parameter("username", Operator.EQUALS, userName));
        Account account = accountDataManager.get(1, new Parameter("uid", Operator.EQUALS, testAccountUid));

        if (user == null) {
            logger.warn(String.format("Creating [%s] user account.", userName));

            Map<String, Object> data = new HashMap<>();
            data.put("username", userName);
            data.put("password", password);
            data.put("admin", false);
            data.put("first", "Admin");
            data.put("last", "Heartzones");
            data.put("verified", new Date());
            data.put("male", false);

            Map<String, Object> rtn = userDataManager.save(1, data);

            AccountUser accountUser = new AccountUser(account.getId());
            accountUser.setUser((Integer) rtn.get("id"));
            accountUser.setRole("admin");

            accountUserDataManager.save(1, accountUser);
        }
    }

    private void userName_caseInsensitive() {
        UserDataManager userDataManager = new UserDataManager(this.properties);
        List<User> users = userDataManager.list(1, new HashSet<>());

        for (User u : users) {
            if (u.getUsername().equals(u.getUsername().toLowerCase())) continue;

            logger.info(String.format("Fixing User.userName [%s] case.", u.getUsername()));

            Map<String, Object> values = new HashMap<>();
            values.put("username", u.getUsername());

            userDataManager.save(1, u.getId(), values);
        }
    }
}

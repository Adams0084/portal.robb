package com.heartzones.startup.jetty;

import com.google.common.base.Strings;
import com.heartzones.ApplicationProperties;
import com.heartzones.ApplicationPropertiesFactory;
import com.heartzones.authentication.Session;
import com.heartzones.authentication.SessionFactory;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Scope;
import javax.inject.Singleton;
import javax.servlet.Servlet;
import java.io.File;
import java.net.URL;

public class JettyInitializer {
    private static final Logger logger = LoggerFactory.getLogger(JettyInitializer.class);
    private ApplicationProperties applicationProperties;

    public JettyInitializer(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void start() throws Exception {

        Integer port = Integer.parseInt(applicationProperties.get("server.port"));
        Server server = new Server(port);

        ServletContextHandler context = new ServletContextHandler(server, "/");

        ErrorHandler errorHandler = new ErrorHandler();
        errorHandler.setShowStacks(Boolean.parseBoolean(applicationProperties.get("server.stacktrace.show", "false")));
        context.setErrorHandler(errorHandler);

        context.addServlet(apiServlet(), "/api/*");
        context.addServlet(webServlet(), "/*");

        //context.addFilter(CacheHeaderResponseFilter.class, "*", EnumSet.of(DispatcherType.ASYNC, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.REQUEST));

        /* adjust connection factory to remove server type in header response*/
        HttpConfiguration httpConfig = new HttpConfiguration();
        httpConfig.setSendServerVersion(false);
        HttpConnectionFactory httpFactory = new HttpConnectionFactory(httpConfig);
        ServerConnector httpConnector = new ServerConnector(server, httpFactory);
        httpConnector.setPort(port);
        server.setConnectors(new Connector[]{httpConnector});

        try {
            logger.info(String.format("JettyInitializer Server starting on port [%d]", port));

            server.start();
            server.join();
        } finally {
            if (server.isStarted()) {
                server.destroy();
            }
        }
    }

    private ServletHolder apiServlet() {
        ResourceConfig config = new ResourceConfig();

        ApplicationPropertiesFactory.properties = applicationProperties;
        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(ApplicationPropertiesFactory.class)
                        .to(ApplicationProperties.class);
            }
        });

        SessionFactory.properties = applicationProperties;
        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(SessionFactory.class)
                        .to(Session.class)
                        .in(Singleton.class);
            }
        });


        config.register(MultiPartFeature.class);
        config.packages(applicationProperties.get("jetty.rest.api.package"));

        Servlet servlet = new ServletContainer(config);
        return new ServletHolder(servlet);
    }

    private ServletHolder webServlet() {
        ServletHolder servletHolder = new ServletHolder("default", new DefaultServlet());

        String webPath = applicationProperties.get("jetty.web.path");
        if (Strings.isNullOrEmpty(webPath))
            webPath = "src/main/webapp";

        // run web assets local if running from local project
        if (new File(webPath).exists()) {
            servletHolder.setInitParameter("resourceBase", webPath);
        } else {
            webPath = "webapp";
            URL resource = this.getClass().getClassLoader().getResource(webPath);
            if (resource != null)
                servletHolder.setInitParameter("resourceBase", resource.toExternalForm());
        }
        servletHolder.setInitParameter("directoriesListed", "false");
        return servletHolder;
    }
}


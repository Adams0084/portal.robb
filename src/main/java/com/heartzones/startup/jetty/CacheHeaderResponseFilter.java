package com.heartzones.startup.jetty;

import com.heartzones.ApplicationProperties;
import com.heartzones.authentication.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CacheHeaderResponseFilter  implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(CacheHeaderResponseFilter.class);

    @Context
    ApplicationProperties applicationProperties;

    @Context
    Session session;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        final HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        Map<String, String> headers = new HashMap<>();

        String target = httpRequest.getRequestURI();

        logger.debug(String.format("target: %s", target));

        if (target.startsWith("/api/") || target.endsWith(".html")) {
            headers.put("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.put("Pragma", "no-cache");
        } else {
            headers.put("Cache-Control", "max-age=604800,public");
        }

        for (String k : headers.keySet()) {
            httpResponse.setHeader(k, headers.get(k));
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}

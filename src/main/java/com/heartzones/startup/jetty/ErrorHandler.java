package com.heartzones.startup.jetty;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.stream.Collectors;

public class ErrorHandler extends ErrorPageErrorHandler {
    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String rtn = new BufferedReader(new InputStreamReader(ErrorHandler.class.getClassLoader().getResourceAsStream("exception.html"))).lines().collect(Collectors.joining());

        rtn = rtn.replace("{{errorMessage}}", (String) request.getAttribute("javax.servlet.error.message"));

        Throwable th = (Throwable) request.getAttribute("javax.servlet.error.exception");

        rtn = rtn.replace("{{exception}}", (isShowStacks() && th != null ? th.getClass().getName() : ""));
        rtn = rtn.replace("{{errorStack}}", writeErrorPageStacks(request));
        rtn = rtn.replace("{{errorStatus}}", String.valueOf(response.getStatus()));
        rtn = rtn.replace("{{requestId}}", (request.getAttribute("request.id") != null ? (String)request.getAttribute("request.id") : ""));

        rtn = rtn.replace("\"exception\":\"\",", "");
        rtn = rtn.replace("\"log\":\"\",", "");
        rtn = rtn.replace(",\"stack\":\"\"", "");

        response.getWriter().append(rtn);

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");

    }

    private String writeErrorPageStacks(HttpServletRequest request)
            throws IOException {
        if (!isShowStacks()) return "";

        StringBuilder sb = new StringBuilder();

        Throwable th = (Throwable) request.getAttribute("javax.servlet.error.exception");
        while (th != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            th.printStackTrace(pw);
            pw.flush();

            sb.append(sw.getBuffer().toString());
            sb.append("/n");
            th = th.getCause();
        }

        return sb.toString();
    }
}

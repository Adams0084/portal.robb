package com.heartzones.data;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MultivaluedMap;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static com.heartzones.data.DataTools.Objects.getFields;

public class DataTools {
    private final static Logger logger = LoggerFactory.getLogger(DataTools.class);
    private final static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss.SSS";
    private final static String dateTimeISOFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private final static String dateFormat = "yyyy-MM-dd";

    public static class Dates {
        public static String formatDate(Date val) {
            if (val == null) return null;

            SimpleDateFormat dt = new SimpleDateFormat(dateFormat);
            return dt.format(val);
        }

        public static String formatDateTime(Date val) {
            if (val == null) return null;

            SimpleDateFormat dt = new SimpleDateFormat(dateTimeFormat);
            return dt.format(val).replace(" 00:00:00.000", "");
        }

        public static String formatISODateTime(Date val) {
            if (val == null) return null;

            SimpleDateFormat dt = new SimpleDateFormat(dateTimeISOFormat);
            return dt.format(val);
        }

        public static Date parse(String val) {
            try {
                if (val == null) return null;

                SimpleDateFormat dt;
                if (val.contains("T"))
                    dt = new SimpleDateFormat(dateTimeISOFormat);
                else if (val.length() > 10)
                    dt = new SimpleDateFormat(dateTimeFormat);
                else
                    dt = new SimpleDateFormat(dateFormat);

                return dt.parse(val);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        public static Date addDays(Date date, int days) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, days);
            return cal.getTime();
        }

        public static Date addHours(Date date, int hours) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.HOUR, hours);
            return cal.getTime();
        }

        public static Date addMinutes(Date date, int minutes) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, minutes);
            return cal.getTime();
        }

        public static Date trimTime(Date date) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            return calendar.getTime();
        }

        public static Date max() {
            return Dates.parse("9999-12-31 23:59:59.000");
        }
    }

    public static class UUIDs {
        public static byte[] toBytes(UUID val) {
            long hi = val.getMostSignificantBits();
            long lo = val.getLeastSignificantBits();
            return ByteBuffer.allocate(16).putLong(hi).putLong(lo).array();
        }

        public static UUID fromBytes(byte[] bytes) {
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            LongBuffer longBuffer = buffer.asLongBuffer();
            long mostSigBits = longBuffer.get(0);
            long leastSigBits = longBuffer.get(1);
            return new UUID(mostSigBits, leastSigBits);
        }
    }

    public static class Persistence {
        public static Set<DataInfo> dataInfoSet(Object obj) {
            Set<DataInfo> rtn = new HashSet<>();
            List<Field> fields = getFields(obj);
            for (Field f : fields) {
                DataInfo dataInfo = f.getAnnotation(DataInfo.class);
                if (dataInfo == null) continue;
                rtn.add(DataInfoImpl.fromDataInfo(f.getName(), dataInfo));
            }
            return rtn;
        }

        public static void mapParameters(List<DataInfo> dataInfoList, Set<Parameter> parameters, ApplicationProperties properties) {
            for (Parameter p : parameters) {
                for (DataInfo dataInfo : dataInfoList) {
                    Set<String> aliases = new HashSet<>();
                    String fieldName = null;

                    aliases.add(dataInfo.name());
                    if (!Strings.isNullOrEmpty(dataInfo.Aliases())) {
                        aliases.addAll(new HashSet<>(Arrays.asList(dataInfo.Aliases().split(","))));
                    }

                    for (String n : aliases) {
                        if (p.getName().equalsIgnoreCase(n)) {
                            fieldName = n;
                            break;
                        }
                    }

                    if (Strings.isNullOrEmpty(fieldName)) continue;

                    p.setName(dataInfo.name());
                    if (dataInfo.StoreLowerCase())
                        p.setValue(((String) p.getValue()).toLowerCase());
                    if (dataInfo.Password())
                        p.setValue(new Encryption(properties).hash((String) p.getValue()));
                    if (dataInfo.Encrypted())
                        p.setValue(new Encryption(properties).encrypt((String) p.getValue()));
                    if (dataInfo.JSON() && !p.getValue().getClass().getName().equals(String.class.getName()))
                        p.setValue(new Gson().toJson(p.getValue()));
                    if (dataInfo.UUIDAsBytes()) {
                        p.setValue(UUIDs.toBytes((UUID) p.getValue()));
                    }
                }
            }
        }

        public static Map<String, Object> from(List<DataInfo> dataInfoList, Map<String, Object> data, ApplicationProperties properties) {
            Map<String, Object> rtn = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

            for (DataInfo dataInfo : dataInfoList) {
                if (dataInfo.Password()) continue;
                if (!data.containsKey(dataInfo.name())) continue;

                Object val = data.get(dataInfo.name());

                if (val != null) {
                    try {
                        if (dataInfo.Password()) val = null;
                        if (dataInfo.Encrypted()) val = new Encryption(properties).decrypt((String) val);
                        if (dataInfo.JSON() && val.getClass().getName().equals(String.class.getName()))
                            val = new Gson().fromJson((String) val, (new TreeMap<String, Object>()).getClass());
                        if (dataInfo.UUIDAsBytes()) {
                            if (((byte[]) val).length == 0) val = null;
                            else val = UUIDs.fromBytes((byte[]) val);
                        }
                    } catch (Exception e) {
                        logger.error(String .format("Error Mapping %s value %s", dataInfo.name(), val));
                        throw e;
                    }
                }
                rtn.put(dataInfo.name(), val);
            }
            return rtn;
        }

        public static Map<String, Object> to(List<DataInfo> dataInfoList, Map<String, Object> data, ApplicationProperties properties) {
            Map<String, Object> rtn = new LinkedHashMap<>();
            for (DataInfo dataInfo : dataInfoList) {
                if (dataInfo.ReadOnly()) continue;

                Set<String> aliases = new HashSet<>();
                String fieldName = null;

                aliases.add(dataInfo.name());
                if (!Strings.isNullOrEmpty(dataInfo.Aliases())) {
                    aliases.addAll(new HashSet<>(Arrays.asList(dataInfo.Aliases().split(","))));
                }

                for (String n : aliases) {
                    if (data.containsKey(n)) {
                        fieldName = n;
                        break;
                    }
                }

                if (Strings.isNullOrEmpty(fieldName)) continue;

                Object val = data.get(fieldName);

                if (val != null) {
                    if (dataInfo.StoreLowerCase()) val = ((String) val).toLowerCase();
                    if (dataInfo.Password()) val = new Encryption(properties).hash((String) val);
                    if (dataInfo.Encrypted()) val = new Encryption(properties).encrypt((String) val);
                    if (dataInfo.JSON() && !val.getClass().getName().equals(String.class.getName()))
                        val = new Gson().toJson(val);
                    if (dataInfo.UUIDAsBytes()) val = UUIDs.toBytes((UUID) val);
                    if (dataInfo.DateOnly() && val.getClass().getName().equals(Date.class.getName()))
                        val = DataTools.Dates.trimTime((Date) val);
                }

                rtn.put(fieldName, val);
            }
            return rtn;
        }
    }

    public static class Objects {
        private static Map<Class, List<Field>> fieldsStore = new HashMap<>();

        public static List<Field> getFields(Object obj) {
            Class clazz = obj.getClass();

            List<Field> fields = new ArrayList<>();

            if (fieldsStore.containsKey(clazz))
                fields = fieldsStore.get(clazz);
            else {
                Class origClazz = clazz;
                while (clazz != null) {
                    fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
                    clazz = clazz.getSuperclass();
                }

                fieldsStore.put(origClazz, fields);
            }

            return fields;
        }

        public static void setValue(Object obj, String fieldName, Object val) {
            try {
                List<Field> fields = getFields(obj);
                Field field = fields.stream().filter(f -> f.getName().equalsIgnoreCase(fieldName)).findFirst().get();

                field.setAccessible(true);
                field.set(obj, val);
                field.setAccessible(false);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static Object getValue(Object obj, String fieldName) {
            try {
                List<Field> fields = getFields(obj);
                Field field = fields.stream().filter(f -> f.getName().equalsIgnoreCase(fieldName)).findFirst().get();

                field.setAccessible(true);
                Object rtn = field.get(obj);
                field.setAccessible(false);

                return rtn;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public static void to(Object object, Map<String, Object> data) {
            List<Field> fields = getFields(object);

            for (Field f : fields) {
                try {
                    DataInfo dataInfo = f.getAnnotation(DataInfo.class);
                    if (dataInfo == null) continue;

                    String fieldKey = null;
                    List<String> fieldAliases = new ArrayList<>(Arrays.asList(dataInfo.Aliases().split(",")));

                    if (!Strings.isNullOrEmpty(dataInfo.name()))
                        fieldAliases.add(0, dataInfo.name());
                    fieldAliases.add(0, f.getName());

                    for (String k : fieldAliases) {
                        if (!Strings.isNullOrEmpty(k)) {
                            if (data.containsKey(k)) {
                                fieldKey = k;
                                break;
                            }
                        }
                    }

                    if (Strings.isNullOrEmpty(fieldKey)) continue;
                    Object val = data.get(fieldKey);

                    if (val != null) {
                        if (dataInfo.JSON() && val.getClass().getName().equals(String.class.getName()))
                            val = new Gson().fromJson((String) val, (new TreeMap<String, Object>()).getClass());
                        if (f.getType().equals(Date.class) && val.getClass().equals(String.class)) {
                            val = DataTools.Dates.parse(val.toString());
                        }

                        f.setAccessible(true);
                        f.set(object, val);
                        f.setAccessible(false);
                    }

                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        public static Map<String, Object> from(Object object) {
            Map<String, Object> rtn = new LinkedHashMap<>();

            List<Field> fields = getFields(object);

            for (Field f : fields) {
                try {
                    f.setAccessible(true);
                    Object val = f.get(object);
                    f.setAccessible(false);

                    if (val != null) {
                        DataInfo dataInfo = f.getAnnotation(DataInfo.class);
                        if (dataInfo != null) {
                            String fieldName = Strings.isNullOrEmpty(dataInfo.name()) ? f.getName() : dataInfo.name();

                            if (dataInfo.JSON() && !val.getClass().getName().equals(String.class.getName()))
                                val = new Gson().toJson(val);

                            rtn.put(fieldName, val);
                        } else {
                            rtn.put(f.getName(), val);
                        }
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }

            return rtn;
        }

        public static Set<Parameter> mapQueryString(MultivaluedMap<String, String> queryParams, Object obj) {
            Set<Parameter> params = new LinkedHashSet<>();

            List<Field> fields = getFields(obj);

            for (String key : queryParams.keySet()) {
                List<String> criteria = queryParams.get(key);

                for (String val : criteria) {
                    Operator op = Operator.EQUALS;

                    String[] keySplit = key.split(Pattern.quote("."));

                    String fieldName = keySplit[0];

                    if (keySplit.length > 1) {
                        String opIndicator = keySplit[1];
                        if (opIndicator.equalsIgnoreCase("starts"))
                            op = Operator.GREATER_THAN_EQUAL_TO;
                        else if (opIndicator.equalsIgnoreCase("ends"))
                            op = Operator.LESS_THAN;
                    }

                    Object value = null;
                    final String searchFieldName = fieldName;
                    Optional<Field> oField = fields.stream().filter(f -> f.getName().equalsIgnoreCase(searchFieldName)).findFirst();
                    if (!oField.isPresent()) {
                        logger.warn(String.format("Unknown field in query [%s] on [%s]", fieldName, obj.getClass().getName()));
                        continue;
                    }

                    Field field = oField.get();

                    DataInfo dataInfo = field.getAnnotation(DataInfo.class);
                    if (dataInfo != null && !Strings.isNullOrEmpty(dataInfo.name())) {
                        fieldName = dataInfo.name();
                    }

                    Class<?> fieldType = field.getType();
                    if (fieldType.equals(String.class))
                        value = val;
                    else if (fieldType.equals(Boolean.class))
                        value = Boolean.parseBoolean(val);
                    else if (fieldType.equals(Integer.class))
                        value = Integer.parseInt(val);
                    else if (fieldType.equals(Double.class))
                        value = Double.parseDouble(val);
                    else if (fieldType.equals(UUID.class))
                        value = UUID.fromString(val);
                    else if (fieldType.equals(Date.class))
                        value = DataTools.Dates.parse(val);
                    else
                        throw new RuntimeException(String.format("Unknown parameter conversion for field [%s] of Type [%s]", fieldName, fieldType.getName()));

                    if (!params.stream().anyMatch(p -> p.getName().equalsIgnoreCase(key))) {
                        params.add(new Parameter(fieldName, op, value));
                    }
                }
            }
            return params;
        }
    }

    public static class DB {
        public static String whereBuilder(Set<Parameter> parameters) {
            return whereBuilder(null, parameters);
        }

        public static String whereBuilder(String tableAlias, Set<Parameter> parameters) {
            StringBuilder where = new StringBuilder();
            where.append(" WHERE ");
            Boolean first = true;
            for (Parameter p : parameters) {
                if (!first) {
                    where.append(" AND ");
                } else {
                    first = false;
                }

                if (!Strings.isNullOrEmpty(tableAlias)) {
                    where.append(tableAlias);
                    where.append(".");
                }
                where.append(p.getName());

                if (p.getValue() == null) {
                    if (p.getOperator().equals(Operator.NOT_EQUALS))
                        where.append(" IS NOT NULL");
                    else
                        where.append("IS NULL");
                } else {
                    if (p.getOperator().equals(Operator.EQUALS))
                        where.append(" = ?");
                    else if (p.getOperator().equals(Operator.NOT_EQUALS))
                        where.append(" <> ?");
                    else if (p.getOperator().equals(Operator.LESS_THAN))
                        where.append(" < ?");
                    else if (p.getOperator().equals(Operator.GREATER_THAN))
                        where.append(" > ?");
                    else if (p.getOperator().equals(Operator.LESS_THAN_EQUAL_TO))
                        where.append(" <= ?");
                    else if (p.getOperator().equals(Operator.GREATER_THAN_EQUAL_TO))
                        where.append(" >= ?");
                    else
                        throw new RuntimeException("Unknown Operator");
                }
            }

            String rtn = where.toString();
            return rtn.trim().equalsIgnoreCase("WHERE") ? "" : rtn;
        }

    }
}

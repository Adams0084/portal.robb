package com.heartzones.data;

import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ConnectionManager {
    private final static Logger logger = LoggerFactory.getLogger(ConnectionManager.class);

    private static Map<String, Connection> _connections = new HashMap<>();
    private ApplicationProperties properties;

    public ConnectionManager(ApplicationProperties properties) {
        this.properties= properties;
    }

    public Connection get() {
        try {
            String url = properties.get("database.url");
            String user = properties.get("database.user");
            String password = properties.get("database.password");
            Integer poolMin = Integer.parseInt(properties.get("database.pool.min"));
            Integer poolMax = Integer.parseInt(properties.get("database.pool.max"));
            Integer poolCount = Integer.parseInt(properties.get("database.pool.count"));

            if (url == null) return null;

            StringBuilder sb = new StringBuilder();
            sb.append(url);
            sb.append(":");
            sb.append(user);
            sb.append(":");
            sb.append(password);
            sb.append(":");
            sb.append(poolMin);
            sb.append(":");
            sb.append(poolMax);
            sb.append(":");
            sb.append(poolCount);

            String key = new Encryption(properties).hash(sb.toString());
            Connection conn = _connections.get(key);

            if (conn == null) {
                conn = new Connection(url, user, password, poolMin, poolMax, poolCount);
                if (conn != null) {
                    _connections.put(key, conn);
                }
            }

            return conn;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
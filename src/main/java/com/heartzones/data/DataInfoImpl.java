package com.heartzones.data;

import com.google.common.base.Strings;

import java.lang.annotation.Annotation;

public class DataInfoImpl implements DataInfo {
    public DataInfoImpl(String name) {
        this.name = name;
    }

    private String name;
    private boolean encrypted = false;
    private boolean password = false;
    private boolean json = false;
    private boolean uuidAsBytes = false;
    private boolean dateOnly = false;
    private boolean readOnly = false;
    private String aliases = "";
    private boolean storeLowerCase = false;

    @Override
    public String name() {
        return name;
    }

    public static DataInfoImpl fromDataInfo(String name, DataInfo dataInfo) {
        DataInfoImpl rtn = new DataInfoImpl(name);

        rtn.name = Strings.isNullOrEmpty(dataInfo.name()) ? name: dataInfo.name();

        rtn.encrypted = dataInfo.Encrypted();
        rtn.readOnly = dataInfo.ReadOnly();
        rtn.password = dataInfo.Password();
        rtn.json = dataInfo.JSON();
        rtn.uuidAsBytes = dataInfo.UUIDAsBytes();
        rtn.aliases = dataInfo.Aliases();
        rtn.dateOnly = dataInfo.DateOnly();
        rtn.storeLowerCase = dataInfo.StoreLowerCase();

        return rtn;
    }

    @Override
    public boolean Encrypted() {
        return encrypted;
    }

    public DataInfoImpl setEncrypted(boolean val) {
        encrypted = val;
        return this;
    }

    @Override
    public boolean Password() {
        return password;
    }

    public DataInfoImpl setPassword(boolean val) {
        password = val;
        return this;
    }

    @Override
    public boolean JSON() {
        return json;
    }

    public DataInfoImpl setJSON(boolean val) {
        json = val;
        return this;
    }

    @Override
    public boolean UUIDAsBytes() {
        return uuidAsBytes;
    }

    public DataInfoImpl setUUIDAsBytes(boolean val) {
        uuidAsBytes = val;
        return this;
    }

    @Override
    public boolean DateOnly() {
        return dateOnly;
    }

    public DataInfoImpl setDateOnly(boolean val) {
        dateOnly = val;
        return this;
    }

    @Override
    public boolean ReadOnly() {
        return readOnly;
    }

    public DataInfoImpl setReadOnly(boolean val) {
        readOnly = val;
        return this;
    }

    @Override
    public String Aliases() {
        return aliases;
    }

    @Override
    public boolean StoreLowerCase() {
        return this.storeLowerCase;
    }

    public DataInfoImpl setAliases(String val) {
        aliases = val;
        return this;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }
}

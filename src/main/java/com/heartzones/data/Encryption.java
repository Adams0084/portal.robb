package com.heartzones.data;

import com.heartzones.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;

public class Encryption {
    private static final Logger logger = LoggerFactory.getLogger(Encryption.class);
    private ApplicationProperties properties;

    public Encryption(ApplicationProperties properties) {
        this.properties = properties;
    }

    public String encrypt(String value) {
        try {
            Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
            byte[] encryptedBytes = cipher.doFinal(value.getBytes());

            return DatatypeConverter.printBase64Binary(encryptedBytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String decrypt(String encrypted) {
        try {
            Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
            byte[] plainBytes = cipher.doFinal(DatatypeConverter.parseBase64Binary(encrypted));

            return new String(plainBytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Cipher getCipher(int cipherMode) {
        Cipher rtn;
        try {
            String encryptionAlgorithm = "AES";
            SecretKeySpec keySpecification = new SecretKeySpec(this.properties.get("encryption.key").getBytes("UTF-8"), encryptionAlgorithm);
            rtn = Cipher.getInstance(encryptionAlgorithm);
            rtn.init(cipherMode, keySpecification);
        } catch (InvalidKeyException e1) {
            if (e1.getMessage().toLowerCase().startsWith("illegal key size")) {
                logKeySizeInvalid();
            }
            throw new RuntimeException(e1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return rtn;
    }
    private void logKeySizeInvalid() {
        logger.warn("Key Size not set correctly!");
        logger.warn("Install Java Cryptography Extensions for you java version");
        logger.warn("http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html");
        logger.warn("Copy: contents of downloaded zip to JDK security folder");
        logger.warn("Mac Example: /Library/Java/JavaVirtualMachines/java-8-oracle/jre/lib/security");
        logger.warn("Ubuntu Example: /usr/lib/jvm/java-8-oracle/jre/lib/security");
    }
    public String hash(String value) {
        try {
            String hashValue = String.format("%s%s", this.properties.get("encryption.hash.salt"), value);

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(hashValue.getBytes(StandardCharsets.UTF_8));

            return DatatypeConverter.printBase64Binary(hash);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String hash(InputStream inputStream) {
        try {
            int len;
            int size = 1024;
            byte[] buf;

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                buf = new byte[size];
                while ((len = inputStream.read(buf, 0, size)) != -1)
                    bos.write(buf, 0, len);
                buf = bos.toByteArray();


            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(buf);

            return DatatypeConverter.printBase64Binary(hash);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
package com.heartzones.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataInfo {
    String name() default "";
    boolean Encrypted() default false;
    boolean Password() default false;
    boolean JSON() default false;
    boolean UUIDAsBytes() default false;
    boolean ReadOnly() default false;
    boolean DateOnly() default false;
    String Aliases() default "";
    boolean StoreLowerCase() default false;
}

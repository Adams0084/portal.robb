package com.heartzones.data;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Connection {
    private static final Logger logger = LoggerFactory.getLogger(Connection.class);

    private String url;
    private String user;
    private String password;
    private Integer poolMin;
    private Integer poolMax;
    private Integer poolCount;

    Connection(String url) {
        this.url = url;
        this.user = this.password = "";
        this.poolMin = 1;
        this.poolMax = 100;
        this.poolCount = 5;
    }

    Connection(String url, String user, String password) {
        this(url);
        this.user = user;
        this.password = password;
    }

    Connection(String url, String user, String password, Integer poolMin, Integer poolMax, Integer poolCount) {
        this(url, user, password);
        this.poolMin = (poolMin == null ? this.poolMin : poolMin);
        this.poolMax = (poolMax == null ? this.poolMax : poolMax);
        this.poolCount = (poolCount == null ? this.poolCount : poolCount);
    }

    void init() {
        try {
            BoneCPConfig config = new BoneCPConfig();

            config.setJdbcUrl(url);
            config.setUser(user);
            config.setPassword(password);
            config.setMinConnectionsPerPartition(poolMin);
            config.setAcquireIncrement(poolMin);
            config.setMaxConnectionsPerPartition(poolMax);
            config.setPartitionCount(poolCount);

            _connection = new BoneCP(config);
        } catch (Exception ex) {
            _connection = null;
            throw new RuntimeException(String.format("Unable to map Connection to [%s]", url), ex);
        }
    }

    BoneCP _connection;

    public java.sql.Connection connection() {
        try {
            if (_connection == null) {
                init();
            }
            return _connection.getConnection();
        } catch (Exception ex) {
            throw new RuntimeException("Connection not Initialized properly.", ex);
        }
    }

    public Integer execute(String statement) {
        return execute(statement, null);
    }

    public Integer execute(String statement, Map<String, Object> parameters) {
        java.sql.Connection conn = null;
        PreparedStatement stmt = null;
        Integer rtn = null;
        try {
            conn = connection();

            stmt = conn.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);
            writeParameters(stmt, parameters);
            stmt.executeUpdate();

            ResultSet auto = stmt.getGeneratedKeys();
            while (auto.next()) rtn = auto.getInt(1);

            stmt.close();
            conn.close();

            return rtn;
        } catch (Exception ex) {
            throw new RuntimeException("Error Executing Query.", ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                throw new RuntimeException("Error closing database connections.", ex);
            }
        }
    }

    public List<Map<String, Object>> executeQuery(String statement) {
        return executeQuery(statement, null);
    }

    public List<Map<String, Object>> executeQuery(String statement, Map<String, Object> parameters) {
        java.sql.Connection conn = null;
        PreparedStatement stmt = null;
        ArrayList<Map<String, Object>> rtn = new ArrayList<>();

        try {
            conn = connection();

            stmt = conn.prepareStatement(statement, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            writeParameters(stmt,parameters);
            ResultSet rs = stmt.executeQuery();

            ResultSetMetaData meta = rs.getMetaData();
            Integer columnCount = meta.getColumnCount();

            while (rs.next()) {
                Map<String, Object> row = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
                for (Integer i = 1; i <= columnCount; i++) {
                    String name = meta.getColumnLabel(i);
                    Object value = rs.getObject(i);

                    if (value != null) {
                        row.put(name, value);
                    }
                }
                rtn.add(row);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception ex) {
            throw new RuntimeException("Error Executing Query.", ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                throw new RuntimeException("Error closing database connection.", ex);
            }
        }

        return rtn;
    }

    private void writeParameters(PreparedStatement stmt, Map<String, Object> parameters) {
        try {
            if (parameters != null && parameters.size() > 0) {
                Integer x = 0;
                for (String k : parameters.keySet()) {
                    x++;
                    Object val = parameters.get(k);
                    if (val == null) stmt.setObject(x, val);
                    else if (val instanceof Boolean) stmt.setBoolean(x, (Boolean) val);
                    else if (val instanceof java.util.Date)
                        stmt.setString(x, DataTools.Dates.formatDateTime((java.util.Date)val));
                    else if (val instanceof Integer) stmt.setInt(x, (Integer) val);
                    else if (val instanceof Long) stmt.setLong(x, (Long) val);
                    else if (val instanceof String) stmt.setString(x, (String) val);
                    else if (val instanceof byte[]) stmt.setBytes(x, (byte[]) val);
                    else {
                        logger.warn(String.format("DataType [%s] unknown in mapping parameters.", val.getClass().getName()));
                        stmt.setString(x, val.toString());
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

package com.heartzones.data;

public class Parameter {
    private String name;
    private Operator operator;
    private Object value;

    public Parameter() {}
    public Parameter(String name, Operator operator, Object value){
        this.name = name;
        this.operator = operator;
        this.value = value;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public Operator getOperator() { return operator; }
    public void setOperator(Operator operator) { this.operator = operator; }

    public Object getValue() { return value; }
    public void setValue(Object value) { this.value = value; }
}

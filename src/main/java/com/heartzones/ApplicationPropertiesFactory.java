package com.heartzones;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ext.Provider;
import java.util.function.Supplier;

@Provider
public class ApplicationPropertiesFactory implements Supplier<ApplicationProperties> {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationPropertiesFactory.class);

    public static ApplicationProperties properties;

    public ApplicationPropertiesFactory() {
        if (properties == null) {
            properties = new ApplicationProperties();
            properties.initialize();
        }
    }

    @Override
    public ApplicationProperties get() {
        return properties;
    }
}

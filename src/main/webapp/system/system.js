page.system = {
    dashboard: {
        load: function() {
            page.system.dashboard.renderFilter();
            page.system.dashboard.executeQuery();

            core.events.add(core.getSingle("[core-name|=accounts]"), "click", function(e) {
                var e = core.events.get(e);
                var src = e.target;

                if (src.hasAttribute("actions")) {
                    var row = core.getParentbyAttribute(src, "row");
                    var rowData = core.data.get(row);

                    if (src.getAttribute("actions") == "extend") {
                        page.system.dashboard.extend(rowData["id"], row);
                    }
                    if (src.getAttribute("actions") == "upgrade") {
                        page.system.dashboard.upgrade(rowData["id"], row);
                    }
                }
            });

            core.events.add(core.getSingle("#accounts-filter"), "click", function(e){
                var e = core.events.get(e);
                var src = e.target;

                if (src.getAttribute("action") == "delete") {
                    var container = core.getParentbyClass(src, "core-input-container");

                    if (!container) return;

                    var data = core.data.get(container);
                    data = core.objects.flatten(data);
                    for (var k in data)
                        delete page.system.dashboard.filter[k];

                    page.system.dashboard.renderFilter();
                    page.system.dashboard.executeQuery();
                }
                else if (src.getAttribute("action") == "add-field-button") {
                    core.className.toggle(core.getSingle("#accounts-filter-fields-list"), "hidden");
                }
                else if (src.getAttribute("action") == "add-field") {
                    var fldName = core.data.element.value(src);
                    page.system.dashboard.filter[fldName] = undefined;
                    page.system.dashboard.renderFilter();
                }
            });
        },
        extend: function(id, row) {
             core.ajax.send({
                "url": "/api/account/license/extend",
                "data": JSON.stringify({"id": id}),
                "type": "POST",
                "success": function (data) {
                    core.data.set(row, data);
                    page.session.load();
                }
           });
        },
        add: function(id, row) {
            core.ajax.send({
                "url": "/api/account/license/extend",
                "data": JSON.stringify({"id": id}),
                "type": "POST",
                "success": function (data) {
                    core.data.set(row, data);
                    page.session.load();
                }
            });
        },
        remove: function(id, row) {
            core.ajax.send({
                "url": "/api/account/license/remove",
                "data": JSON.stringify({"id": id}),
                "type": "POST",
                "success": function (data) {
                    core.data.set(row, data);
                    page.session.load();
                }
            });
        },
        upgrade: function(id, row) {
             var el = row;
             core.ajax.send({
                "url": "/api/account/license/upgrade",
                "data": JSON.stringify({"id": id}),
                "type": "POST",
                "success": function (data) {
                    core.data.set(row, data);
                    page.session.load();
                    var action = core.get("[actions|=extend]", el);
                    core.className.add(action, "hidden");

                    var upgrade = core.get("[actions|=upgrade]", el);
                    core.className.add(upgrade, "hidden");
                }
           });
        },
        filterMeta: {
            "type": {"type": "select", "list":"account.types","name":"type","label": "Type"},
            "name": {"type": "text", "name":"name","validation": "exists", "label": "Account Name"},
            "owner_email": {"type": "text", "name":"owner_email","validation": "exists, email", "label": "Owner Email"},
            "owner_first": {"type": "text", "name":"owner_first","validation": "exists", "label": "Owner First"},
            "owner_last": {"type": "text", "name":"owner_last","validation": "exists", "label": "Owner Last"},
            "licenses": {"type": "select", "name":"licenses","validation": "exists", "label": "License Type", "list": "license.types"},
            "expires.starts": {"type": "text", "name":"expires.starts","validation": "exists", "label": "License Expires", "format":"isoDate"},
            "expires.ends": {"type": "text", "name":"expires.ends","validation": "exists", "label": "License Expires", "format":"isoDate"},
            "created.starts": {"type": "text", "name":"created.starts","validation": "exists", "label": "Account Created", "format":"isoDate"},
            "created.ends": {"type": "text", "name":"created.ends","validation": "exists", "label": "Account Created", "format":"isoDate"},
        },
        filter: {
            "licenses": "Temporary License",
            "expires.starts": core.format.to(new Date(), "isoDate")
        },
        renderFilter: function() {
            var templateText = '<div class="core-input-container"><div class="label">{label}</div><input class="input simple" type="text" core-name="{name}" autocomplete="off" core-validate="{validation}" core-format="{format"/><i action="delete" class="fa fa-times-circle"></i> <div class="clearFloats"></div></div>'
            var templateSelect= '<div class="core-input-container"><div class="label">{label}</div><select class="input simple" type="text" core-name="{name}" autocomplete="off" core-list="{list}" core-validate="{validation}" core-format="{format"></select><i action="delete" class="fa fa-times-circle"></i><div class="clearFloats"></div></div>'
            var addField = '<i action="add-field-button" class="fa fa-plus"></i>';
            var fieldList = '<div id="accounts-filter-fields-list" class="hidden" core-name="fields"></div>';
            var field = '<div action="add-field">{name}</div>';

            var filterElement = core.getSingle("#accounts-filter");

            core.empty(filterElement);
            core.append(filterElement, addField);
            core.append(filterElement, fieldList);

            var unUsedFields = core.objects.clone(page.system.dashboard.filterMeta);
            for (var i in unUsedFields) {
                if (page.system.dashboard.filter.hasOwnProperty(i))
                    delete unUsedFields[i];
            }
            var fldList = core.getSingle("#accounts-filter-fields-list");
            for (var i in unUsedFields) {
                var fld = core.strings.replace(field, "{name}", i);
                core.append(fldList, fld);
            }

            for (var i in page.system.dashboard.filter) {
                var meta = page.system.dashboard.filterMeta[i];
                if (!i) continue;

                var el;
                switch (meta.type) {
                    case "select":
                        el = templateSelect;
                        break;
                    default:
                        el = templateText;
                        break;
                }

                el = core.strings.replace(el, "{label}", meta.label);
                el = core.strings.replace(el, "{name}", meta.name);
                el = core.strings.replace(el, "{validation}", meta.validation);
                el = core.strings.replace(el, "{format}", meta.format);
                el = core.strings.replace(el, "{list}", meta.list);

                core.append(filterElement, el);
            }

            core.data.set(filterElement, page.system.dashboard.filter);

            core.get("input, select", filterElement, function(){
                core.events.add(this, "blur", function(e){
                    var e = core.events.get(e);
                    var src = e.target;

                    var container = core.getParentbyClass(src, "core-input-container");
                    if (!container) return;

                    var data = core.data.get(container);
                    data = core.objects.flatten(data);

                    page.system.dashboard.filter = core.extend(page.system.dashboard.filter, data);
                    page.system.dashboard.executeQuery();
                });
            });
        },
        executeQuery: function(e) {
            core.ajax.send({
                "url": "/api/accounts",
                "type": "GET",
                "data": page.system.dashboard.filter,
                "success": function (data) {
                    var container = core.getSingle("[core-name|=accounts]");
                    core.empty(container);
                    core.data.set(container, data, function(e, row){
                        if (row.licenses) {
                            if (row.licenses.indexOf("Temporary License") > -1) {
                                var action = core.get("[actions|=extend]", e);
                                core.className.remove(action, "hidden");

                                var upgrade = core.get("[actions|=upgrade]", e);
                                core.className.remove(upgrade, "hidden");
                            }
                        }
                    });
                }
            });
        }
    },
    schools: {
        load: function () {

        }
    },
    businesses: {
        load: function () {

        }
    },
    individuals: {
        load: function () {

        }
    },
    reports: {
        load: function () {

        }
    },
    settings: {
        load: function() {

        }
    }
};

page.session.validate("system", function() {
    core.tabstrip.init(core.getSingle("#system-menu"), core.getSingle("#system-context"));

    var qs = core.queryString.get();
    if (core.is.numeric(qs.user)) page.profile.user = qs.user;

    var action = "dashboard";
    if (qs.action) action = qs.action;

    action = "#system-" + action;
    var action = core.getSingle(action);
    if (!action) action = core.getSingle("#system-dashboard");
    core.tabstrip.select(action);

});
if (!page) var page = {};

page.profile = function() {
    var rtn = {
        user: undefined,
        pages: {
            general: "/profile/general.htm"
        },
        elements: {
            context: undefined,
            menu: undefined
        },
        general: {
            data: undefined,
            load: function() {
                var url = "/api/profile";
                if (page.profile.user) url = url + "/" + page.profile.user;

                core.ajax.send({
                "url": url,
                "type": "GET",
                "success": function (data) {
                    page.profile.general.data = core.objects.clone(data);

                    if (data.male === false || data.male === true) data.male = data.male.toString();

                    core.data.set(page.profile.elements.context, data);
                }});
            },
            save: function() {
                if (!core.validation.validate(page.profile.elements.context)) return;

                var data = core.data.get(page.profile.elements.context);

                if (data.male === "true" || data.male === "false") data.male = core.cast.boolean(data.male);

                var orig = page.profile.general.data;
                for (var k in orig) {
                    if (data.hasOwnProperty(k)) {
                        if (data[k] === orig[k]) {
                            delete data[k];
                        }
                        else if (orig[k] !== undefined && orig[k] !== ""  && orig[k] !== null) {
                            if (data[k] === undefined || data[k] === "" || data[k] === null)
                                data[k] = null;
                        }
                    }
                }
                for (var k in data) {
                    if (data[k] === undefined || data[k] === "")
                        delete data[k];
                }

                if (Object.keys(data).length) {
                    // save it
                    data["id"] = orig.id;
                }

                core.ajax.send({
                    "url": "/api/profile",
                    "data": core.objects.flatten(data),
                    "type": "POST",
                    "success": function (rtn) {
                        page.session.load();
                    }
                });
            }
        },
        account: {
            load: function() {
            }
        }
    };

    rtn.elements.menu = core.getSingle("#profile-menu");
    rtn.elements.context = core.getSingle("#profile-context");

    return rtn;
}.call();

page.session.validate(function() {
    core.tabstrip.init(core.getSingle("#profile-menu"), core.getSingle("#profile-context"));

    var qs = core.queryString.get();
    if (core.is.numeric(qs.user)) page.profile.user = qs.user;

    var action = "general";
    if (qs.action) action = qs.action;

    action = "#profile-" + action;
    var action = core.getSingle(action);
    if (!action) action = core.getSingle("#profile-general");
    core.tabstrip.select(action);
});


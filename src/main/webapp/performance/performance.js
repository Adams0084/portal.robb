page.performance = {
    elements: {
        menu: core.getSingle("#menu"),
        context: core.getSingle("#context"),
        filter: core.getSingle("#filter")
    },
    summary: {
        load: function() {

        },
        filterMeta: {
            "type": {"type": "select", "list":"account.types","name":"type","label": "Type"},
            "name": {"type": "text", "name":"name","validation": "exists", "label": "Account Name"},
            "owner_email": {"type": "text", "name":"owner_email","validation": "exists, email", "label": "Owner Email"},
            "owner_first": {"type": "text", "name":"owner_first","validation": "exists", "label": "Owner First"},
            "owner_last": {"type": "text", "name":"owner_last","validation": "exists", "label": "Owner Last"},
            "licenses": {"type": "select", "name":"licenses","validation": "exists", "label": "License Type", "list": "license.types"},
            "expires.starts": {"type": "text", "name":"expires.starts","validation": "exists", "label": "License Expires", "format":"isoDate"},
            "expires.ends": {"type": "text", "name":"expires.ends","validation": "exists", "label": "License Expires", "format":"isoDate"},
            "created.starts": {"type": "text", "name":"created.starts","validation": "exists", "label": "Account Created", "format":"isoDate"},
            "created.ends": {"type": "text", "name":"created.ends","validation": "exists", "label": "Account Created", "format":"isoDate"},
        },
        filter: {
            "licenses": "Temporary License",
            "expires.starts": core.format.to(new Date(), "isoDate")
        },
        renderFilter: function() {
            var templateText = '<div class="core-input-container"><div class="label">{label}</div><input class="input simple" type="text" core-name="{name}" autocomplete="off" core-validate="{validation}" core-format="{format"/><i action="delete" class="fa fa-times-circle"></i> <div class="clearFloats"></div></div>'
            var templateSelect= '<div class="core-input-container"><div class="label">{label}</div><select class="input simple" type="text" core-name="{name}" autocomplete="off" core-list="{list}" core-validate="{validation}" core-format="{format"></select><i action="delete" class="fa fa-times-circle"></i><div class="clearFloats"></div></div>'
            var addField = '<i action="filter-add-field-button" class="fa fa-plus"></i>';
            var fieldList = '<div id="filter-fields-list" class="hidden" core-name="fields"></div>';
            var field = '<div action="filter-add-field">{name}</div>';

            var filterElement = page.performance.elements.filter;

            core.empty(filterElement);
            core.append(filterElement, addField);
            core.append(filterElement, fieldList);

            var unUsedFields = core.objects.clone(page.system.accounts.filterMeta);
            for (var i in unUsedFields) {
                if (page.system.accounts.filter.hasOwnProperty(i))
                    delete unUsedFields[i];
            }
            var fldList = core.getSingle("#filter-fields-list");
            for (var i in unUsedFields) {
                var fld = core.strings.replace(field, "{name}", i);
                core.append(fldList, fld);
            }

            for (var i in page.system.performance.filter) {
                var meta = page.system.performance.filterMeta[i];
                if (!i) continue;

                var el;
                switch (meta.type) {
                    case "select":
                        el = templateSelect;
                        break;
                    default:
                        el = templateText;
                        break;
                }

                el = core.strings.replace(el, "{label}", meta.label);
                el = core.strings.replace(el, "{name}", meta.name);
                el = core.strings.replace(el, "{validation}", meta.validation);
                el = core.strings.replace(el, "{format}", meta.format);
                el = core.strings.replace(el, "{list}", meta.list);

                core.append(filterElement, el);
            }

            core.data.set(filterElement, page.system.accounts.filter);

            core.get("input, select", filterElement, function(){
                core.events.add(this, "blur", function(e){
                    var e = core.events.get(e);
                    var src = e.target;

                    var container = core.getParentbyClass(src, "core-input-container");
                    if (!container) return;

                    var data = core.data.get(container);
                    data = core.objects.flatten(data);

                    page.system.accounts.filter = core.extend(page.system.accounts.filter, data);
                    page.system.accounts.executeQuery();
                });
            });
        },
        executeQuery: function(e) {
            core.ajax.send({
                "url": "/api/performance/summary",
                "type": "GET",
                "data": page.system.performance.filter,
                "success": function (data) {
                    var container = core.getSingle("[core-name|=data]");
                    core.empty(container);
                    core.data.set(container, data, function(e, row){

                    });
                }
            });
        }
    }
}

page.session.validate("system", function() {
    core.tabstrip.init(page.performance.elements.menu, page.performance.elements.context);

    var qs = core.queryString.get();
    if (core.is.numeric(qs.user)) page.profile.user = qs.user;

    var action = "summary";
    if (qs.action) action = qs.action;

    action = "#menu-" + action;
    var action = core.getSingle(action);
    if (!action) action = core.getSingle("#menu-summary");
    core.tabstrip.select(action);
});
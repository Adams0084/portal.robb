core.each(core.get("[id=\"loginButton\"]"), function () {
    core.events.add(this, "click", login);
});

core.events.add(core.getSingle("#registerLink"), "click", function() {
    window.location.href = "/register/";
});

core.events.add(core.getSingle("#reset-password"), "click", function() {
    window.location.href = "/login/forgot-password.html";
});
core.events.add(core.getSingle("#request-reset-password"), "click", resetPassword);
core.getSingle("#username").focus();

core.events.add(core.getSingle("#loginForm"), "keyup", function (e) {
    if (e.keyCode === 13)
        login();
});

function login() {
    if (!core.validation.validate(core.getSingle("#loginForm"))) return;

    var data = core.data.get(core.getSingle("#loginForm"));

    var redirect = core.queryString.get();
    redirect = redirect["redirect"];

    core.ajax.send({
        "url": "/api/auth/login",
        "data": data,
        "type": "POST",
        "success": function (data) {
            core.security.user(data);

            page.session.home(redirect);
        },
        "error": function(data) {
            if (data.status == 401) {
                core.prompt.show(
                {
                    title: "Login Exception",
                    message: "Username or Password invalid",
                    modal: true,
                    buttons: [
                    {
                        text: "ok",
                        position: "right"
                    }]
                });
            }
        }
    });
}
function resetPassword() {
    var data = core.data.get(core.getSingle("#ResetForm"));

    if (!data.username) {
        core.alert("Email Address must be populated to request a reset password email.");
    }

    core.ajax.send({
        "url": "/api/auth/reset-password/" + data.username,
        "type": "GET",
        "success": function (data) {
            core.alert("An Email has been sent to the email provided if it is an account.  Please follow the link in that email to reset your password.");
        }
    });
}


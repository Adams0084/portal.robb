var page = page || {};
if (!page.elements) page.elements = {};
page.elements.container = core.getSingle(".core-container");
core.views.version("${timestamp}");


var query = core.queryString.get();
if (query["header"] === undefined || core.cast.boolean(query["header"])) {
    if (!core.className.check(page.elements.container, "header-off")) {
        core.className.add(page.elements.container, "header-on");
        core.views.get("/_partials/header.htm", page.elements.container);
    }
}
else {
    core.className.add(page.elements.container, "header-off");
}
/*
if (query["footer"] === undefined || core.cast.boolean(query["footer"])) {
    if (!core.className.check(page.elements.container, "footer-off")) {
        core.className.add(page.elements.container, "footer-on");
        core.views.get("/_partials/footer.htm", page.elements.container);
    }
}
else {
    core.className.add(page.elements.container, "footer-off");
}
*/

core.className.add(page.elements.container, "footer-off");

page.session = {
    home: function(redirect) {
        if (redirect) {
            window.location.href = redirect;
            return;
        }
        if (core.security.role("system")) {
            window.location.href = "/system/";
            return;
        }
        if (core.security.role("owner")) {
            window.location.href = "/admin/";
            return;
        }
        if (core.security.role("admin")) {
            window.location.href = "/admin/";
            return;
        }
        if (core.security.role("teacher")) {
            window.location.href = "/admin/";
            return;
        }
        if (core.security.role("student")) {
            window.location.href = "/student/";
            return;
        }

        redirect = core.queryString.get();
        redirect = redirect["redirect"];

        if (!redirect)
            window.location.href = "/login/";
        else
            window.location.href = "/login/?redirect=" + redirect;
    },
    get: function(callback) {
        var session = core.security.user();
        if (!session) {
            page.session.load(callback);
        }
        else {
            if (core.is.function(callback)) {
                callback.call();
            }
        }

        return session;
    },
    load: function() {
        var callback;
        var refresh;
        for (var i = 0; i < arguments.length; i++) {
            if (core.is.function(arguments[i])) callback = arguments[i];
            if (core.is.boolean(arguments[i])) refresh = arguments[i];
        }

        core.ajax.send({
            url: "/api/auth/session" + (refresh ? "/refresh" : ""),
            type: "GET",
            success: function (data) {
                core.security.clear();

                core.security.user(data);

                if (data.authenticated && !data.verified) {
                    core.ajax.send({
                        "url": "/api/auth/verify",
                        "type": "GET",
                        "success": function (data) {
                            core.notify.info("An Email has been sent to verify your account.  Please follow the link provided in that email.");

                            if (core.is.function(callback)) {
                                callback.call();
                            }
                        }
                    });
                }
                else {
                    if (core.is.function(callback)) {
                        callback.call();
                    }
                }
            }
        });
    },
    validate: function() {
        var callback;
        var role;
        for (var i = 0; i < arguments.length; i++) {
            if (core.is.function(arguments[i])) callback = arguments[i];
            if (core.is.string(arguments[i])) role = arguments[i];
        }

        page.session.get(function() {
            var user = core.security.user();
            if (!user) {
                page.auth.login();
                return;
            }
            if (role && !core.security.role(role)) {
                location.href = '/';
                return;
            }
            if (callback) {
                callback.call();
            }
        });
    }
}

core.notify.initialize({position: "bottom,right", parent: page.elements.container });

core.get("[authenticated]", function () {
    core.className.add(this, "hidden-authentication");
});

core.pubsub.subscribe("core.security.publishes.userChange", function(user) {
    core.get("[authenticated]", function () {
        var val = core.cast.boolean(this.getAttribute("authenticated"));
        user = user || {};
        if (val) {
            if (user.authenticated)
                core.className.remove(this, "hidden-authentication");
            else
                core.className.add(this, "hidden-authentication");
        }
        else {
            if (user.authenticated)
                core.className.add(this, "hidden-authentication");
            else
                core.className.remove(this, "hidden-authentication");
        }
    });

    core.data.set(page.elements.container, {session:user});
});

core.pubsub.publish("core.security.publishes.userChange", core.security.user());

page.auth = {
    login: function() {
        var url = "/login/";
        if (location.pathname != "/" && location.pathname != "/login/")
            url = core.strings.replace("/login/?redirect={url}", "{url}", location.pathname);
        location.href = url;
    },
    logout: function() {
        core.prompt.show(
        {
            title: "Logout ?",
            message: "Would you like to logout of the application ?",
            modal: true,
            buttons: [
            {
                text: "YES",
                position: "left",
                success: function() {
                    core.ajax.send({
                    url: "/api/auth/logout",
                    type: "GET",
                    success: function (data) {
                        core.security.clear();
                        setTimeout("window.location='/';", 50);
                    }
                    });
                }
            },
            {
                text: "NO",
                position: "right"
            }]
        });
    }
}

core.pubsub.subscribe(core.ajax.publishes.error, function(data) {
    if (data.status == 401) {
        if (location.pathname == "/login/") return;
        page.session.load(function() {
            var user = core.security.user();
            if (!user || !user.authenticated)
                page.auth.login();
        });
    }
});

core.pubsub.subscribe(core.publishes.pulse, function() {
    if (location.pathname.substring(0, 7) == "/login/") return;
    if (location.pathname == "/") return;
    if (location.pathname == "/register/") return;

    function _checkExpiration() {
        var user = core.security.user();
        if (!user || !user.authenticated) return false;

        var now = new Date();
        var expires = new Date(user["session.expiration"]);

        if (expires < now) return false;

        return true;
    }

    if (_checkExpiration() === false) {
        page.session.load(function () {
            if (_checkExpiration() === false)
                page.auth.login();
        });
    }
});

core.lists.set("account.types", [
    {value: "", text: "-- Account Type --"},
    {value: "Educational", text: "Educational"},
    {value: "Business", text: "Business"},
    {value: "Individual", text: "Individual"},
]);

core.lists.set("license.types", [
    {value: "", text: "-- License Type --"},
    {value: "Education License", text: "Education"},
    {value: "Business License", text: "Business"},
    {value: "Individual License", text: "Individual"},
    {value: "Temporary License", text: "Temporary"}
]);
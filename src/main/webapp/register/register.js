core.getSingle("#first").focus();
core.get("#registerButton",function(){
   core.className.add(this,"hidden");
});

core.get("input[type='checkbox']", core.getSingle("#registration-selection"), function () {
    core.events.add(this, "click", function () {
        var selected = this;
        core.get("input[type='checkbox']", core.getSingle("#registration-selection"), function () {
            if (this !== selected) {
                this.removeAttribute("checked");
                this.checked = false;
            }
            else {
                this.setAttribute("checked", "checked");
                this.checked = true;
            }
        });

        var view = selected.getAttribute("core-view");

        core.empty(core.getSingle("#registration-panel"));
        core.views.get(view, core.getSingle("#registration-panel"));
    });
});

core.events.fire(core.getSingle("#registration-education"), "click");

core.events.add(core.getSingle("#btnSignUp"), "click", function () {
    if (!core.validation.validate(core.getSingle(".core-body"))) return;

    var data = core.data.get(core.getSingle(".core-body"));

    core.ajax.send({
        "url": "/api/auth/registration",
        "data": core.objects.flatten(data),
        "type": "POST",
        "success": function (rtn) {
            core.security.user(rtn);

            window.location.href = "/";
        }
    });
});

core.events.add(core.getSingle("#btnLogin"), "click", page.auth.login);

package com.heartzones;

import com.google.common.base.Strings;
import com.heartzones.models.Email;
import com.heartzones.processor.EmailProcessor;
import com.heartzones.startup.Liquibase;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailProcessorTest {
    private static Logger logger = LoggerFactory.getLogger(EmailProcessorTest.class);
    private static ApplicationProperties properties;

    @BeforeClass
    public static void init() {
        properties = Utilities.getTestProperties();

        Liquibase liquibase = new Liquibase();
        liquibase.start(properties);
    }

    @Test
    public void send() {
        String emailTo = properties.get("test.email");
        Assume.assumeFalse("property [test.email] is not configured in the file /src/test/resources/my.properties", Strings.isNullOrEmpty(emailTo));

        EmailProcessor.start(properties);
        EmailProcessor.Async = false;

        Email email = new Email();

        email.setRecipiant(emailTo);
        email.setSubject("HZ SendGrid Test");
        email.setBody("Hi there");
        email.setSender("test@bca.net");

        EmailProcessor.process(properties, email);
    }
}

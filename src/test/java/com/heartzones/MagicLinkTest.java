package com.heartzones;

import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.MagicLinkDataManager;
import com.heartzones.models.MagicLink;
import com.heartzones.startup.Liquibase;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Date;
import java.util.UUID;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MagicLinkTest {
    private static UUID link;
    private static String value;
    private static ApplicationProperties properties;

    @BeforeClass
    public static void init() {
        properties = Utilities.getTestProperties();

        value = UUID.randomUUID().toString();
        link = UUID.randomUUID();

        Liquibase liquibase = new Liquibase();
        liquibase.start(properties);
    }

    @Test
    public void _01_write() {
        MagicLink magic = new MagicLink();
        magic.setLink(link);
        magic.setAction("test-Action");
        magic.setExpires(DataTools.Dates.addDays(new Date(), 3));
        magic.getData().put("test",value);

        MagicLinkDataManager magicLinkDataManager = new MagicLinkDataManager(MagicLinkTest.properties);
        magicLinkDataManager.save(1, magic);
    }
    @Test
    public void _02_read() {
        MagicLinkDataManager magicLinkDataManager = new MagicLinkDataManager(MagicLinkTest.properties);
        MagicLink magic = magicLinkDataManager.get(1, new Parameter("link", Operator.EQUALS, link));

        Assert.assertNotNull("returned MagicLink is empty", magic);

        String val = (String)magic.getData().get("test");
        Assert.assertEquals("test data comparison", val, value);
    }
}

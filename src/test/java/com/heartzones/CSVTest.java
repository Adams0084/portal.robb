package com.heartzones;


import com.heartzones.utilities.CSV;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

public class CSVTest {
    @Test
    public void test_no_quote() {
        Reader line = new StringReader("10,AU,Australia");
        List<List<String>> result;
        try {
            result = CSV.parse(line);
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).size() == 3);
        Assert.assertTrue(result.get(0).get(0).equals("10"));
        Assert.assertTrue(result.get(0).get(1).equals("AU"));
        Assert.assertTrue(result.get(0).get(2).equals("Australia"));
    }
    @Test
    public void test_no_quote_but_double_quotes_in_column() {
        Reader line = new StringReader("10,AU,Aus\"\"tralia");
        List<List<String>> result;
        try {
            result = CSV.parse(line);
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).size() == 3);
        Assert.assertTrue(result.get(0).get(0).equals("10"));
        Assert.assertTrue(result.get(0).get(1).equals("AU"));
        Assert.assertTrue(result.get(0).get(2).equals("Aus\"tralia"));
    }
    @Test
    public void test_double_quotes() {
        Reader line = new StringReader("\"10\",\"AU\",\"Australia\"");
        List<List<String>> result;
        try {
            result = CSV.parse(line);
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).size() == 3);
        Assert.assertTrue(result.get(0).get(0).equals("10"));
        Assert.assertTrue(result.get(0).get(1).equals("AU"));
        Assert.assertTrue(result.get(0).get(2).equals("Australia"));
    }
    @Test
    public void test_double_quotes_but_double_quotes_in_column() {
        Reader line = new StringReader("\"10\",\"AU\",\"Aus\"\"tralia\"");
        List<List<String>> result;
        try {
            result = CSV.parse(line);
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).size() == 3);
        Assert.assertTrue(result.get(0).get(0).equals("10"));
        Assert.assertTrue(result.get(0).get(1).equals("AU"));
        Assert.assertTrue(result.get(0).get(2).equals("Aus\"tralia"));
    }
    @Test
    public void test_double_quotes_but_comma_in_column() {
        Reader line = new StringReader("\"10\",\"AU\",\"Aus,tralia\"");
        List<List<String>> result;
        try {
            result = CSV.parse(line);
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).size() == 3);
        Assert.assertTrue(result.get(0).get(0).equals("10"));
        Assert.assertTrue(result.get(0).get(1).equals("AU"));
        Assert.assertTrue(result.get(0).get(2).equals("Aus,tralia"));
    }
    @Test
    public void test_mixed_quotes() {
        Reader line = new StringReader("10,AU,\"Aus,tralia\"");
        List<List<String>> result;
        try {
            result = CSV.parse(line);
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).size() == 3);
        Assert.assertTrue(result.get(0).get(0).equals("10"));
        Assert.assertTrue(result.get(0).get(1).equals("AU"));
        Assert.assertTrue(result.get(0).get(2).equals("Aus,tralia"));
    }
}

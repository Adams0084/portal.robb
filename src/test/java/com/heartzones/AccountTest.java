package com.heartzones;

import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountDataManager;
import com.heartzones.models.Account;
import com.heartzones.models.AccountTypes;
import com.heartzones.startup.Liquibase;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.UUID;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountTest {
    private static Integer id;
    private static ApplicationProperties properties;

    @BeforeClass
    public static void init() {
        properties = Utilities.getTestProperties();

        Liquibase liquibase = new Liquibase();
        liquibase.start(properties);
    }

    @Test
    public void _01_write() {
        Account account = new Account();

        account.setType(AccountTypes.Educational.name());
        account.setUid(UUID.randomUUID());
        account.setName("test account");
        account.setAddress_1("Address 1");
        account.setAddress_2("Address 2");
        account.setCity("City");
        account.setState("st");
        account.setZip("99999");

        AccountDataManager accountDataManager = new AccountDataManager(properties);
        accountDataManager.save(1, account);

        id = account.getId();
    }

    @Test
    public void _02_read_by_id() {
        AccountDataManager accountDataManager = new AccountDataManager(properties);
        Account account = accountDataManager.get(1, new Parameter("id", Operator.EQUALS, id));

        Assert.assertNotNull("returned account is empty", account);
        Assert.assertEquals("Resulting key is different ?", id, account.getId());
    }
}

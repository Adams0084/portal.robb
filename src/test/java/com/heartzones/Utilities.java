package com.heartzones;

import com.heartzones.ApplicationProperties;

import java.io.File;

class Utilities {
    private static ApplicationProperties applicationProperties;

    static ApplicationProperties getTestProperties() {
        if (applicationProperties == null) {
            applicationProperties = new ApplicationProperties();

            applicationProperties.load(new File("src/test/resources/test.properties"));
            applicationProperties.load(new File("src/test/resources/my.properties"));

            applicationProperties.put("database.drop", "true");
        }
        return applicationProperties;
    }
}

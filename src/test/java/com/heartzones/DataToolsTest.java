package com.heartzones;

import com.heartzones.data.DataTools;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;

public class DataToolsTest {
    @Test
    public void DateConverterTest() {

        Date DT = new Date();

        String sDT = DataTools.Dates.formatDateTime(DT);

        Date vDT = DataTools.Dates.parse(sDT);

        Assert.assertEquals("Date not same after formatDateTime and parse", DT, vDT);
    }
    @Test
    public void DateAddDaysTest() {
        Date DT = new Date();

        Date DT2 = DataTools.Dates.addDays(DT, 2);

        long diff = DT2.getTime() - DT.getTime();
        long diffHours = diff / (60 * 60 * 1000);

        Assert.assertEquals("add days not right", diffHours, 48);
    }
    @Test
    public void DateAddHoursTest() {
        Date DT = new Date();

        Date DT2 = DataTools.Dates.addHours(DT, 4);

        long diff = DT2.getTime() - DT.getTime();
        long diffHours = diff / (60 * 60 * 1000);

        Assert.assertEquals("add days not right", diffHours, 4);
    }

    @Test
    public void UUIDtoBytesTest() {
        UUID val = UUID.randomUUID();

        byte[] bytes = DataTools.UUIDs.toBytes(val);
        UUID rVal = DataTools.UUIDs.fromBytes(bytes);

        Assert.assertEquals("UUID and final UUID different", val, rVal);
    }
    @Test
    public void DateMaxTest() {
        Date dt = DataTools.Dates.max();
        Date now = new Date();

        Assert.assertNotNull("DataTools.Dates.max() returned null", dt);
        Assert.assertNotEquals("DataTools.Dates.max() returned same year", dt.getYear(), now.getYear());
    }
}

package com.heartzones;

import com.heartzones.data.Connection;
import com.heartzones.data.ConnectionManager;
import com.heartzones.data.DataTools;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseTest {
    private static ConnectionManager connectionManager;

    private static Map<String, Object> testData;
    private static Map<String, Object> testData2;

    @BeforeClass
    public static void init() {
        connectionManager = new ConnectionManager(Utilities.getTestProperties());
    }

    @Test
    public void _01_createTable() {
        String statement = "CREATE TABLE test (id int NOT NULL AUTO_INCREMENT PRIMARY KEY, created DATETIME NULL, bin BINARY(16) NULL, val VARCHAR(255));";

        Connection conn = connectionManager.get();
        conn.execute(statement);
    }

    @Test
    public void _02_insertIntoTable() {
        UUID val = UUID.randomUUID();

        String statement = "INSERT INTO test (created, bin, val) VALUES (?,?,?);";
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("created", new Date());
        params.put("bin", DataTools.UUIDs.toBytes(val));
        params.put("val", val.toString());


        Connection conn = connectionManager.get();
        Integer id = conn.execute(statement, params);

        params.put("id", id);
        testData = params;

        Assert.assertTrue("id has a value", id > 0);
    }

    @Test
    public void _03_selectFromTable() {
        String statement = "SELECT * FROM test WHERE id = ?;";
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("id", testData.get("id"));

        Connection conn = connectionManager.get();
        List<Map<String, Object>> rows = conn.executeQuery(statement, params);

        Assert.assertTrue("Select Statement produced rows.", rows.size() == 1);

        Map<String,Object> row = rows.get(0);

        Assert.assertNotEquals("created changed", row.get("created"), testData.get("created"));
        Assert.assertNotEquals("bin changed", row.get("bin"), testData.get("bin"));
        Assert.assertNotEquals("val changed", UUID.fromString((String)row.get("val")), testData.get("val"));
        Assert.assertNotEquals("bin/val changed", DataTools.UUIDs.fromBytes((byte[])row.get("bin")), testData.get("val"));
    }

    @Test
    public void _04_UpdateRecordTable() {
        UUID val = UUID.randomUUID();
        String statement = "UPDATE test SET bin = ?, val = ? WHERE id = ?;";

        Map<String, Object> params = new LinkedHashMap<>();
        params.put("bin", DataTools.UUIDs.toBytes(val));
        params.put("val", val.toString());
        params.put("id", testData.get("id"));

        Connection conn = connectionManager.get();
        conn.execute(statement, params);

        testData2 = params;
    }

    @Test
    public void _05_selectFromTableUpdated() {
        String statement = "SELECT * FROM test WHERE id = ?;";
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("id", testData.get("id"));

        Connection conn = connectionManager.get();
        List<Map<String, Object>> rows = conn.executeQuery(statement, params);

        Assert.assertTrue("Select Statement (Updated) produced rows.", rows.size() == 1);

        Map<String,Object> row = rows.get(0);

        Assert.assertNotEquals("created changed", row.get("created"), testData.get("created"));
        Assert.assertNotEquals("bin changed", row.get("bin"), testData2.get("bin"));
        Assert.assertNotEquals("val changed", UUID.fromString((String)row.get("val")), testData2.get("val"));
        Assert.assertNotEquals("bin/val changed", DataTools.UUIDs.fromBytes((byte[])row.get("bin")), testData2.get("val"));
    }

    @Test
    public void _06_dropTable() {
        String statement = "DROP TABLE test;";

        Connection conn = connectionManager.get();
        conn.execute(statement);
    }
}




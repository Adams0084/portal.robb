package com.heartzones;

import com.heartzones.data.DataTools;
import com.heartzones.data.Operator;
import com.heartzones.data.Parameter;
import com.heartzones.dataManagers.AccountDataManager;
import com.heartzones.dataManagers.AccountLicenseDataManager;
import com.heartzones.dataManagers.UserDataManager;
import com.heartzones.models.Account;
import com.heartzones.models.AccountLicense;
import com.heartzones.models.User;
import com.heartzones.startup.Liquibase;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataManagerTest {
    private static ApplicationProperties properties;
    private static Integer accountId;
    private static Integer accountLicenseId;

    @BeforeClass
    public static void init() {
        properties = Utilities.getTestProperties();

        Liquibase liquibase = new Liquibase();
        liquibase.start(properties);    }

    @Test
    public void _01_simple_read() {
        // Read default System user(1) created by liquibase scripts

        UserDataManager userDataManager = new UserDataManager(properties);

        User u = userDataManager.get(1, new Parameter("id", Operator.EQUALS, 1));

        Assert.assertNotNull("Request resulted in null object", u);
    }
    @Test
    public void _02_simple_update() {
        // User default System user(1) created by liquibase scripts

        String newValue = "test";
        UserDataManager userDataManager = new UserDataManager(properties);

        Map<String, Object> values = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        values.put("first", newValue);

        userDataManager.save(1, 1, values);

        User u = userDataManager.get(1, new Parameter("id", Operator.EQUALS, 1));

        Assert.assertNotNull("Request resulted in null object", u);
        Assert.assertEquals("Request resulted in no change", u.getFirst(), newValue);
    }
    @Test
    public void _03_simple_insert() {

        UserDataManager userDataManager = new UserDataManager(properties);

        User u = new User();

        u.setFirst("testFirst");
        u.setLast("testLast");
        u.setMiddle("testMiddle");
        u.setUsername("testUserName");

        userDataManager.save(1, u);

        User rtn = userDataManager.get(1, new Parameter("id", Operator.EQUALS, u.getId()));

        Assert.assertNotNull("Request resulted in null object", u);

        Assert.assertEquals("Field Check Failed (created)", u.getCreated(), rtn.getCreated());
        Assert.assertNull("Field null Check Failed (updated)", u.getUpdated());
        Assert.assertNull("Field null Check Failed result (updated)", rtn.getUpdated());
        Assert.assertEquals("Field Check Failed (create_user)", u.getCreate_user(), rtn.getCreate_user());
        Assert.assertNull("Field null Check Failed (update_user)", u.getUpdate_user());
        Assert.assertNull("Field null Check Failed result (update_user)", rtn.getUpdate_user());

        Assert.assertEquals("Field Check Failed (First)", u.getFirst(), rtn.getFirst());
        Assert.assertEquals("Field Check Failed (Last)", u.getLast(), rtn.getLast());
        Assert.assertEquals("Field Check Failed (Middle)", u.getMiddle(), rtn.getMiddle());
        Assert.assertEquals("Field Check Failed (UserName)", u.getUsername().toLowerCase(), rtn.getUsername());
    }
    @Test
    public void _04_account_write() {
        Account a = new Account();

        a.setName("test account name");

        AccountDataManager accountDataManager = new AccountDataManager(properties);

        accountDataManager.save(1, a);
        Account rtn = accountDataManager.get(1, new Parameter("id", Operator.EQUALS, a.getId()));

        Assert.assertNotNull("New Account not in database", rtn);
        Assert.assertEquals("Field Check Failed (name)", a.getName(), rtn.getName());

        accountId = rtn.getId();
    }
    @Test
    public void _05_accountLicense_write() {
        AccountLicense al = new AccountLicense(accountId);

        al.setName(properties.get("account.license.temp.name"));
        al.setExpires(DataTools.Dates.addDays(new Date(), 14));
        al.setFeatures(properties.get("account.license.temp.features"));

        AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);

        accountLicenseDataManager.save(1, al);

        accountLicenseId = al.getId();

        AccountLicense rtn = accountLicenseDataManager.get(1, new Parameter("id", Operator.EQUALS, accountLicenseId));

        Assert.assertNotNull("New Account License not in database", rtn);
        Assert.assertEquals("Field Check Failed (name)", al.getName(), rtn.getName());

        accountId = rtn.getId();
    }

    @Test
    public void _06_accountLicense_delete() {
        AccountLicenseDataManager accountLicenseDataManager = new AccountLicenseDataManager(properties);
        AccountLicense rtn = accountLicenseDataManager.get(1, new Parameter("id", Operator.EQUALS, accountLicenseId));

        Assert.assertNotNull("New Account License not in database", rtn);

        accountLicenseDataManager.delete(1, new Parameter("id", Operator.EQUALS, accountLicenseId));

        rtn = accountLicenseDataManager.get(1, new Parameter("id", Operator.EQUALS, accountLicenseId));
        Assert.assertNull("New Account License is in database", rtn);
    }

}

package com.heartzones;

import com.heartzones.data.Encryption;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class EncryptionTest {
    private static ApplicationProperties properties;

    @BeforeClass
    public static void init() {
        properties = Utilities.getTestProperties();
    }

    @Test
    public void hashTest() {
        String value = "testValue";

        Encryption encryption = new Encryption(properties);

        String hash = encryption.hash(value);
        String hash2 = encryption.hash(value);

        Assert.assertNotNull("hash should not be null", hash);
        Assert.assertNotEquals("value and hash should not equal", value, hash);
        Assert.assertEquals("repeatable test", hash, hash2);
    }

    @Test
    public void EncryptionTest() {
        String value = "testValue";

        Encryption encryption = new Encryption(properties);

        String encrypted = encryption.encrypt(value);
        String decrypted = encryption.decrypt(encrypted);

        Assert.assertNotNull("encryped value should not be null", encrypted);
        Assert.assertNotEquals("value and encryped value should not equal", value, encrypted);
        Assert.assertEquals("repeatable test", decrypted, value);
    }
}
